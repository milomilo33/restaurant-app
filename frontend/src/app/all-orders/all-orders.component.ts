import {Component, Inject, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {AddTableDialogue} from "../restaurant-layout/restaurant-layout.component";
import {AddOrderDialogue} from "../restaurant-layout-waiter/restaurant-layout-waiter.component";
import {ToastrService} from "ngx-toastr";

export interface DialogData {
  order: any
}

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css']
})
export class AllOrdersComponent implements OnInit {

  orders: any;
  criteria: string;
  tables: any;
  selectedTable: any;
  currentPage: number;
  totalPages: number;
  resultsPerPage: number;
  displayedOrders: any;

  constructor(public dialog: MatDialog, private http: HttpClient,private toastr: ToastrService) {
    //this.orders = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
    this.orders = [];
    this.criteria = "Any";
    this.tables = [];
    this.selectedTable = "any";
    this.currentPage = 1;
    this.resultsPerPage = 10;
    this.totalPages = 1;
    this.displayedOrders = [];

    this.initTables();
    this.initOrders();
  }

  initTables(){
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    this.http.get<any>('/api/layout/getAllTables', {
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    }).subscribe(
      data => {
        this.tables = data;
        this.tables.sort((a: number, b: number) => (a > b) ? 1 : -1);
        this.tables = ["any",...this.tables];
      }
    )
  }

  initOrders(){
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    this.http.get<any>('/api/orders/search?criteria=any&table=any', {
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    }).subscribe(
      data => {
        this.orders = data;
        this.totalPages = Math.ceil(this.orders.length / this.resultsPerPage);
        if(this.totalPages <= 0)
          this.totalPages = 1;
        this.initDisplayedOrders();
      },
      error => { }
    )
  }

  ngOnInit(): void {
  }

  initDisplayedOrders(){
    this.displayedOrders = this.orders.slice((this.currentPage - 1)*this.resultsPerPage, this.currentPage*this.resultsPerPage)
  }

  search(){
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    this.http.get<any>('/api/orders/search?criteria=' + this.criteria + '&table=' + this.selectedTable,{
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    }).subscribe(
      data => {
        this.orders = data;
        this.totalPages = Math.ceil(this.orders.length / this.resultsPerPage);
        if(this.totalPages <= 0)
          this.totalPages = 1;
        this.initDisplayedOrders();
      },
      error => {}
    )
  }

  nextPage(){
    this.currentPage += 1;
    if(this.currentPage > this.totalPages)
      this.currentPage = this.totalPages;
    else
      this.initDisplayedOrders()
  }

  prevPage(){
    this.currentPage -= 1;
    if(this.currentPage < 1)
      this.currentPage = 1;
    else
      this.initDisplayedOrders()
  }

  //-------------------------------------------

  openOrderDetails(orderDetails: any){
    const dialogRef = this.dialog.open(OrderDetails, {
      width: '750px',
      height: '750px',
      data: {order: orderDetails},
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        for (let i = 0; i < this.orders.length; i++) {
          if (this.orders[i].id === result.order.id) {
            this.orders[i] = result.order;
            break;
          }
        }
        this.initDisplayedOrders();
      }
    });
  }

  chargeOrder(id: any){
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    this.http.get<any>('/api/orders/isOrderChargeable?id=' + id,{
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    }).subscribe(result => {if(result === false) this.toastr.error("All order items of an order must be charged in order to charge the entire order!")
      else{
      this.http.post<any>('/api/orders/chargeOrder?id=' + id,{}, {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      }).subscribe(result => {if(result)this.orders.find((order: any) => order.id === result.id).completed = result.completed; this.initDisplayedOrders(); this.toastr.success("Order charged!")}, error => {});
    }
    }, error => {});

  }

}

@Component({
  selector: 'order-details',
  templateUrl: 'order-details.html',
  styleUrls: ['./all-orders.component.css']
})
export class OrderDetails {

  displayedOrderItemColumns: string[] = ['name', 'amount', 'status', 'note', 'serve', 'charge', 'cancel', 'deny'];
  orderItems: any;

  constructor(
    public dialogRef: MatDialogRef<OrderDetails>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private http: HttpClient,
    private toastr: ToastrService
  ){
    this.initOrderItems();
  }

  initOrderItems(){
    this.orderItems = this.data.order.orderItems;
    this.orderItems.sort((a: any, b: any) => (a.id > b.id) ? 1 : -1);
  }

  //----------------------------------
  doOrderItem(id: any, type: string, msgToaster: string){
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    this.http.post<any>('/api/orderItems/' + type +'OrderItem?id=' + id,{}, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    }).subscribe(dataE => {this.data.order = dataE; this.initOrderItems();
      this.toastr.success("Order item has been " + msgToaster + "!");
    }, error => {})
  }

  serveOrderItem(id: any){
    this.doOrderItem(id, 'serve', 'served');
  }

  chargeOrderItem(id: any){
    this.doOrderItem(id, 'charge', 'charged');
  }

  cancelOrderItem(id: any){
    this.doOrderItem(id, 'cancel', 'canceled');
   }

  denyOrderItem(id: any){
    this.doOrderItem(id, 'deny', 'denied');
  }
  //----------------------------------

  onNoClick(): void {
    this.dialogRef.close();
  }
}
