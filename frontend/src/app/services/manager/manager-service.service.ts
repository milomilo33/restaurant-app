import {Injectable} from '@angular/core';

import {Observable} from "rxjs";
import {EmployeesTableItem} from "../../employees-table/employees-table-datasource";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {SortDirection} from "@angular/material/sort";

@Injectable({
  providedIn: 'root'
})
export class ManagerServiceService {
  get baseRoute(): string {
    return this._baseRoute;
  }

  set baseRoute(value: string) {
    this._baseRoute = value;
  }

  private _baseRoute: string = '/api/employee'


  constructor(private http: HttpClient) {
  }

  public getEmployees(pageIndex: number | undefined = 0, pageSize: number | undefined = 10, direction: SortDirection | undefined = 'asc', active: string | undefined = 'username'
    , search: string | undefined = "", type: string | undefined = "ALL", wage: number | undefined = 0, greater: string | undefined = "GREATER"
  ): Observable<EmployeesTableItem[]> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.get<EmployeesTableItem[]>(this._baseRoute + 's?'
      + 'pageIndex=' + pageIndex
      + '&pageSize=' + pageSize
      + '&direction=' + direction
      + '&active=' + active
      + '&search=' + search
      + '&type=' + type
      + '&wage=' + wage
      + '&greater=' + greater
      , {headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)});
  }

  getEmployee(data: string): Observable<EmployeesTableItem> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    return this.http.get<EmployeesTableItem>(this._baseRoute + '?username=' + data, {headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)})
  }

  putEmployee(data: EmployeesTableItem): Observable<EmployeesTableItem> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      })
    };

    return this.http.put<EmployeesTableItem>(this._baseRoute, JSON.stringify(data), httpOptions);
  }

  postEmployee(user: EmployeesTableItem): Observable<EmployeesTableItem> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      })
    };

    return this.http.post<EmployeesTableItem>(this._baseRoute, JSON.stringify(user), httpOptions);

  }

  deleteEmployee(username: String): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    return this.http.delete(this._baseRoute + '/' + username, {
      responseType: 'text',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)
    });
  }

  getTotalEmployeeCount(): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    return this.http.get(`${this.baseRoute}/count`, {headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)});
  }
}
