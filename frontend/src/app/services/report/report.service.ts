import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: HttpClient) {
  }

  public getDataRole(role: String = 'barman', start: Date, end: Date) {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    return this.http.get<any>('/api/reports/role?'
      + "role=" + role +
      "&start_date=" + start.toISOString().split("T")[0]
      + "&end_date=" + end.toISOString().split("T")[0], {headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)})
  }

  getDataUsername(role: String = 'barman', start: Date, end: Date) {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    return this.http.get<any>('/api/reports/employee?'
      + "username=" + role +
      "&start_date=" + start.toISOString().split("T")[0]
      + "&end_date=" + end.toISOString().split("T")[0], {headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)})
  }

  getMenuItemNames(): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    return this.http.get('/api/reports/menu-item-names', {headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)});
  }

  getDataMenuItem(menuItem: string | undefined, start: Date, end: Date) {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    return this.http.get<any>('/api/reports/menu-item?'
      + "name=" + menuItem +
      "&start_date=" + start.toISOString().split("T")[0]
      + "&end_date=" + end.toISOString().split("T")[0], {headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)})
  }

  getDataMenuItemPrice(menuItem: string | undefined, start: Date, end: Date) {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    return this.http.get<any>('/api/reports/menu-item-price?'
      + "name=" + menuItem +
      "&start_date=" + start.toISOString().split("T")[0]
      + "&end_date=" + end.toISOString().split("T")[0], {headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)})
  }

  getDataMenuItemPrepare(menuItem: string | undefined, start: Date, end: Date) {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    return this.http.get<any>('/api/reports/menu-item-prepare?'
      + "name=" + menuItem +
      "&start_date=" + start.toISOString().split("T")[0]
      + "&end_date=" + end.toISOString().split("T")[0], {headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)})
  }
}
