import { TestBed } from '@angular/core/testing';

import { CreateMenuItemService } from './create-menu-item.service';

describe('CreateMenuItemService', () => {
  let service: CreateMenuItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateMenuItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
