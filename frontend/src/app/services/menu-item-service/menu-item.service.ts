import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable, map, catchError } from 'rxjs';
import { MenuItemSearchResults } from 'src/app/models/menu-item-search';

@Injectable({
  providedIn: 'root'
})
export class MenuItemService {
  constructor(private http: HttpClient) { }

  createMenuItem(formData: FormData): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.post(`/api/menu-item`, formData, {
      responseType: 'json',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    })
    // .pipe(
    //   map((data) => {
    //     console.log("MAP in request")
    //    return data;
    //   }),
    //   catchError((err) => {
    //     console.log("CATCHERROR in request")
    //    console.error(err);
    //    throw err;
    //   })
    // );
  }

  getMenuItemCategories(): Observable<string[]> {
    // const req = new HttpRequest('GET', `/api/menu-item/categories`, {
    //   responseType: 'json',
    //   headers: new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`),
    // });
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.request<string[]>('GET', `/api/menu-item/categories`, {
      responseType: 'json',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    });
    // .pipe(
    //   map((data) => {
    //    return data;
    //   }),
    //   catchError((err) => {
    //    console.error(err);
    //    throw err;
    //   })
    // );
  }

  public searchSortFilterMenuItems(name: string, allergens: string, typeCriteria: string, availableCriteria: string, category: string, sortOption: string,
                      sortAscOrDesc: string, pageIndex: number, pageSize: number): Observable<MenuItemSearchResults> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.get<MenuItemSearchResults>('/api/menu-item/search?'
      + 'pageIndex=' + pageIndex
      + '&pageSize=' + pageSize
      + '&name=' + name
      + '&allergens=' + allergens
      + '&typeCriteria=' + typeCriteria
      + '&availableCriteria=' + availableCriteria
      + '&category=' + category
      + '&sortAscOrDesc=' + sortAscOrDesc
      + '&sortOption=' + sortOption, {
        responseType: 'json',
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      }
    );
  }

  deleteMenuItem(id: number): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.request<string[]>('GET', `/api/menu-item/delete/${id}`, {
      responseType: 'json',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    });
  }

  updateMenuItem(formData: FormData): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.post(`/api/menu-item/update`, formData, {
      responseType: 'json',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    })
  }

  public searchSortFilterMenuItemsForMenu(name: string, allergens: string, typeCriteria: string, availableCriteria: string, category: string, sortOption: string,
                      sortAscOrDesc: string, pageIndex: number, pageSize: number, inMenuCriteria: string, menuId: number): Observable<MenuItemSearchResults> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.get<MenuItemSearchResults>('/api/menu-item/searchForMenu?'
      + 'pageIndex=' + pageIndex
      + '&pageSize=' + pageSize
      + '&name=' + name
      + '&allergens=' + allergens
      + '&typeCriteria=' + typeCriteria
      + '&availableCriteria=' + availableCriteria
      + '&category=' + category
      + '&sortAscOrDesc=' + sortAscOrDesc
      + '&sortOption=' + sortOption
      + '&inMenuCriteria=' + inMenuCriteria
      + '&menuId=' + menuId, {
        responseType: 'json',
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      }
    );
  }

  removeMenuItemFromMenu(menuId: number, menuItemId: number): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.request<string[]>('GET', `/api/menu-item/remove-from-menu?menuId=${menuId}&menuItemId=${menuItemId}`, {
      responseType: 'json',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    });
  }

  addMenuItemToMenu(menuId: number, menuItemId: number, price: number): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.request<string[]>('GET', `/api/menu-item/add-to-menu?menuId=${menuId}&menuItemId=${menuItemId}&price=${price}`, {
      responseType: 'json',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    });
  }

  updatePriceForMenuItemInMenu(menuId: number, menuItemId: number, price: number): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.request<string[]>('GET', `/api/menu-item/update-in-menu?menuId=${menuId}&menuItemId=${menuItemId}&price=${price}`, {
      responseType: 'json',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    });
  }

}