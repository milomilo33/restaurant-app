import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable, map, catchError } from 'rxjs';
import { MenuSearchResults } from 'src/app/models/menu-search';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  constructor(private http: HttpClient) { }

  createMenu(formData: FormData): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.post(`/api/menu`, formData, {
      responseType: 'json',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    })
  }

  public searchSortFilterMenus(name: string, activeCriteria: string, sortAscOrDesc: string, 
                                pageIndex: number, pageSize: number): Observable<MenuSearchResults> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.get<MenuSearchResults>('/api/menu/search?'
      + 'pageIndex=' + pageIndex
      + '&pageSize=' + pageSize
      + '&name=' + name
      + '&activeCriteria=' + activeCriteria
      + '&sortAscOrDesc=' + sortAscOrDesc, {
        responseType: 'json',
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      }
    );
  }

  deleteMenu(id: number): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.request<string[]>('GET', `/api/menu/delete/${id}`, {
      responseType: 'json',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    });
  }

  updateMenu(id: number, name: string): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.get(`/api/menu/update?id=${id}&name=${name}`, {
      responseType: 'json',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    })
  }

  activateMenu(id: number): Observable<any> {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];

    return this.http.request<string[]>('GET', `/api/menu/activate/${id}`, {
      responseType: 'json',
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    });
  }

}