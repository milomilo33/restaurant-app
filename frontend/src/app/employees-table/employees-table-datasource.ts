import {DataSource} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Observable, of as observableOf} from 'rxjs';
import {ManagerServiceService} from "../services/manager/manager-service.service";

// TODO: Replace this with your own data model type
export interface EmployeesTableItem {
  firstName: string;
  lastName: string;
  emailAddress: string;
  username: string;
  type: string;
  wage: number;
  phoneNumber: string;
}


/**
 * Data source for the EmployeesTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class EmployeesTableDataSource extends DataSource<EmployeesTableItem> {
  dataSource: EmployeesTableItem[] = [];
  paginator: MatPaginator | undefined;
  sort: MatSort | undefined;

  constructor(private _managerService: ManagerServiceService) {
    super();
  }


  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<EmployeesTableItem[]> {
    // if (this.paginator && this.sort) {
    //   // Combine everything that affects the rendered data into one update
    //   // stream for the data-table to consume.
    //   return merge(observableOf(this.dataSource), this.paginator.page, this.sort.sortChange)
    //     .pipe(map(() => {
    //       return this.dataSource;
    //     }));
    // } else {
    //   throw Error('Please set the paginator and sort on the data source before connecting.');
    // }
    return observableOf(this.dataSource);
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect(): void {
  }

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  // private getPagedData(data: EmployeesTableItem[]): EmployeesTableItem[] {
  //   if (this.paginator) {
  //     const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
  //     return data.splice(startIndex, this.paginator.pageSize);
  //
  //   } else {
  //     return data;
  //   }
  // }
  //
  // /**
  //  * Sort the data (client-side). If you're using server-side sorting,
  //  * this would be replaced by requesting the appropriate data from the server.
  //  */
  // private getSortedData(data: EmployeesTableItem[]): EmployeesTableItem[] {
  //   if (!this.sort || !this.sort.active || this.sort.direction === '') {
  //     return data;
  //   }
  //   return data.sort((a, b) => {
  //     const isAsc = this.sort?.direction === 'asc';
  //     switch (this.sort?.active) {
  //       case 'firstName':
  //         return compare(a.firstName, b.firstName, isAsc);
  //       case 'lastName':
  //         return compare(a.lastName, b.lastName, isAsc);
  //       case 'emailAddress':
  //         return compare(a.emailAddress, b.emailAddress, isAsc);
  //       case 'type':
  //         return compare(a.type, b.type, isAsc);
  //       case 'wage':
  //         return compare(a.wage, b.wage, isAsc);
  //       case 'phoneNumber':
  //         return compare(a.phoneNumber, b.phoneNumber, isAsc);
  //       default:
  //         return 0;
  //     }
  //   });
  // }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: string | number, b: string | number, isAsc: boolean): number {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
