import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {EmployeesTableItem} from './employees-table-datasource';
import {ManagerServiceService} from "../services/manager/manager-service.service";
import {MatDialog} from "@angular/material/dialog";
import {UserModalComponent} from "../user-modal/user-modal.component";
import {ActivatedRoute} from '@angular/router';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-employees-table',
  templateUrl: './employees-table.component.html',
  styleUrls: ['./employees-table.component.css']
})
export class EmployeesTableComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<EmployeesTableItem>;
  dataSource: MatTableDataSource<EmployeesTableItem>;
  totalCount: number = 0;
  pIndex: number = 0;
  pSize: number = 10;
  isAdmin: boolean = true;

  roles: string[] = [
    "BARMAN", "WAITER", "CHEF"
  ]

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = [
    'firstName',
    'lastName',
    'emailAddress',
    'username',
    'type',
    'wage',
    'phoneNumber',
    'actions'
  ];
  searchForm = new FormGroup(
    {
      search: new FormControl(""),
      type: new FormControl("ALL"),
      wage: new FormControl(0, Validators.min(0)),
      greater: new FormControl("GREATER")
    }
  )

  constructor(private _managerService: ManagerServiceService,
              public dialog: MatDialog,
              private route: ActivatedRoute) {
    this.dataSource = new MatTableDataSource<EmployeesTableItem>();
    if (this.route.snapshot.data['user'] === 'manager') {
      this._managerService.baseRoute = 'api/staff'


    } else {
      this._managerService.baseRoute = 'api/employee'
      this.roles = ['MANAGER', ...this.roles]
    }
  }

  ngOnInit() {
    if (this.route.snapshot.data['user'] === 'manager') {
      this._managerService.baseRoute = 'api/staff'

    } else {
      this._managerService.baseRoute = 'api/employee'
      this.roles = ['MANAGER', ...this.roles]
      this.isAdmin = false;
    }
    this.dataSource = new MatTableDataSource<EmployeesTableItem>();
    this.fetchData();
    this.dataSource.sort = this.sort;
    //this.dataSource.paginator = this.paginator;
    this.searchForm.valueChanges.subscribe(
      (r) => {
        this.fetchData();
        return r;
      }
    )
  }

  getEmployees() {
    this._managerService.getEmployees(this.paginator?.pageIndex, this.paginator?.pageSize, this.sort?.direction, this.sort?.active).subscribe(
      (data: EmployeesTableItem[]) => {
        this.dataSource.data = data;
        this.table.renderRows();
        return data;
      }
    )
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    //this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
    this._managerService.getTotalEmployeeCount().subscribe(
      (r) => {
        this.totalCount = r;
        return r;
      }
    )
  }

  goToEdit(row: string) {
    const dialogRef = this.dialog.open(UserModalComponent,
      {
        data: [row, this.isAdmin],
        width: "290px",
        height: "740px"
      }
    );
    dialogRef.afterClosed().subscribe((r) => {
      this.fetchData()
      return r;
    })
  }

  addNewEmployee() {
    const dialogRef = this.dialog.open(UserModalComponent,
      {
        data: ["", this.isAdmin],
        width: "290px",
        height: "790px",
      }
    );

    dialogRef.afterClosed().subscribe((r) => {
      this.fetchData();
      return r;
    })
  }

  delete(username: string) {
    console.log(username)
    this._managerService.deleteEmployee(username).subscribe(
      (r) => {
        this.fetchData();
        return r;
      }
    )
  }

  pageChanged(event: PageEvent) {
    this.pIndex = event?.pageIndex;
    this.pSize = event?.pageSize;
    console.log(event)
    this.fetchData();
  }

  fetchData() {

    if (this.route.snapshot.data['user'] === 'manager') {
      this._managerService.baseRoute = 'api/staff'


    } else {
      this._managerService.baseRoute = 'api/employee'
      this.roles = ['MANAGER', ...this.roles]
    }

    this._managerService.getEmployees(this.pIndex, this.pSize, this.sort?.direction, this.sort?.active,
      this.searchForm.get('search')?.value,
      this.searchForm.get('type')?.value,
      this.searchForm.get('wage')?.value,
      this.searchForm.get('greater')?.value
    ).subscribe(
      (data: EmployeesTableItem[]) => {
        this.dataSource.data = data;
        this.table.renderRows();
        this._managerService.getTotalEmployeeCount().subscribe(
          (r) => {
            this.totalCount = r;
            this.table.renderRows();
            return r;
          }
        )
        return data;
      }
    )


  }

}
