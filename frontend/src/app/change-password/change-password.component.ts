import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth-service/auth.service";
import {FormControl, FormGroup} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  constructor(private auth_service: AuthService, private toastr: ToastrService, private router: Router) {
  }


  username: String = JSON.parse(
    atob(localStorage.getItem("user")!.split(".")[1])
  ).username;

  loginForm = new FormGroup(
    {
      old_password: new FormControl(''),
      new_password: new FormControl('')
    }
  );

  ngOnInit(): void {
  }

  changePassword() {
    const change_password = {
      'oldPassword': this.loginForm.get('oldPassword')?.value,
      'newPassword': this.loginForm.get('newPassword')?.value,
      'username': this.username
    }

    this.auth_service.change_password(change_password).subscribe({
      next: (result) => {
        this.toastr.success("Successful login!");
        localStorage.setItem("user", JSON.stringify(result));
        this.findUserRole();
      },
      error: (error) => {
        this.toastr.error("Invalid credentials!");
      }
    });
  }


  findUserRole(): void {
    var userRole = JSON.parse(
      atob(localStorage.getItem("user")!.split(".")[1])
    ).role;

    switch (userRole) {
      case "ROLE_ADMIN":
        console.log("Role: ROLE_ADMIN");
        this.router.navigate(["/restaurant-layout"]);
        break;
      case "ROLE_BARMAN":
        console.log("Role: ROLE_BARMAN");
        this.router.navigate(["/order-items/staff"]);
        break;
      case "ROLE_CHEF":
        console.log("Role: ROLE_CHEF");
        this.router.navigate(["/order-items/staff"]);
        break;
      case "ROLE_HEAD_CHEF":
        console.log("Role: ROLE_HEAD_CHEF");
        //this.router.navigate(["/order-items/staff"]);
        break;
      case "ROLE_WAITER":
        console.log("Role: ROLE_WAITER");
        this.router.navigate(["/restaurant-layout-waiter"]);
        break;
      case "ROLE_MANAGER":
        console.log("Role: ROLE_MANAGER");
        // this.router.navigate(["/some/manager/route"]);
        break;
      default:
        console.log("INVALID ROLE!");
        break;
    }
  }
}
