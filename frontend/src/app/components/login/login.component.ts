import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {Login} from "src/app/models/login";
import {AuthService} from "src/app/services/auth-service/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService
  ) {
    // this.loginForm = this.fb.group({
    //   username: [null, Validators.required],
    //   password: [null, Validators.required],
    // });
    this.loginForm = new FormGroup(
      {
        username: new FormControl(''),
        password: new FormControl('')
      }
    );
  }

  ngOnInit(): void {
  }

  login(): void {
    const auth: Login = {
      username: this.loginForm.value.username,
      password: this.loginForm.value.password,
    };

    this.authService.login(auth).subscribe({
      next: (result) => {
        this.toastr.success("Successful login!");
        localStorage.setItem("user", JSON.stringify(result));
        this.findUserRole();
      },
      error: (error) => {
        this.toastr.error("Invalid credentials!");
      }
    });
  }

  findUserRole(): void {
    var userRole = JSON.parse(
      atob(localStorage.getItem("user")!.split(".")[1])
    ).role;

    switch (userRole) {
      case "ROLE_ADMIN":
        console.log("Role: ROLE_ADMIN");
        this.router.navigate(["/restaurant-layout"]);
        break;
      case "ROLE_BARMAN":
        console.log("Role: ROLE_BARMAN");
        this.router.navigate(["/order-items/staff"]);
        break;
      case "ROLE_CHEF":
        console.log("Role: ROLE_CHEF");
        this.router.navigate(["/order-items/staff"]);
        break;
      case "ROLE_HEAD_CHEF":
        console.log("Role: ROLE_HEAD_CHEF");
        //this.router.navigate(["/order-items/staff"]);
        break;
      case "ROLE_WAITER":
        console.log("Role: ROLE_WAITER");
        this.router.navigate(["/restaurant-layout-waiter"]);
        break;
      case "ROLE_MANAGER":
        console.log("Role: ROLE_MANAGER");
        this.router.navigate(["/manager/reports"]);
        break;
      default:
        console.log("INVALID ROLE!");
        break;
    }
  }

}
