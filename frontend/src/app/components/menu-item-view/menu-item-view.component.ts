import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MenuItemService } from 'src/app/services/menu-item-service/menu-item.service';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import { MenuItemSearchResults } from 'src/app/models/menu-item-search';
import { MenuItem } from 'src/app/models/menu-item';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-menu-item-view',
  templateUrl: './menu-item-view.component.html',
  styleUrls: ['./menu-item-view.component.css']
})
export class MenuItemViewComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  totalCount: number = 0;
  pIndex: number = 0;
  pSize: number = 10;

  menuItems = [];
  name = '';
  allergens = '';
  typeCriteria = 'Any';
  availableCriteria = 'Any';
  categories: string[] = [];
  selectedCategory = 'Any';
  selectedSortOption = 'name';
  sortAscOrDesc = 'asc';
  // currentPage = 1;
  // totalPages = 1;
  // resultsPerPage = 10;
  displayedMenuItems: MenuItem[] = [];

  menuId = -1;
  menuName = '';
  mode = 'All';

  priceForUpdate = -1;
  inMenuCriteria = 'Any';

  constructor(private http: HttpClient, private menuItemService: MenuItemService, private toastr: ToastrService, private router: Router, public dialog: MatDialog) {}

  initCategories() {
    this.menuItemService.getMenuItemCategories().subscribe({
      next: (result) => {
        this.categories = result;
      }
    });
  }

  ngOnInit(): void {
    this.initCategories();
    if (this.router.url.includes('menuitemsformenu')) {
      this.mode = 'Menu';
      this.initForMenu();
    }
    else
      this.fetchDataAll();
  }

  pageChanged(event: PageEvent) {
    this.pIndex = event?.pageIndex;
    this.pSize = event?.pageSize;
    console.log(event)
    if (this.mode === 'All')
      this.fetchDataAll();
    else if (this.mode === 'Menu')
      this.fetchDataMenu();
  }

  fetchDataAll() {
    this.menuItemService.searchSortFilterMenuItems(this.name, this.allergens, this.typeCriteria, 
      this.availableCriteria, this.selectedCategory, this.selectedSortOption, this.sortAscOrDesc, 
      this.pIndex, this.pSize).subscribe({
        next: (data: MenuItemSearchResults) => {
          this.displayedMenuItems = data.menuItemsInPage;
          this.totalCount = data.totalElements;
          return data;
        }
      });
  }

  fetchDataMenu() {
    this.menuItemService.searchSortFilterMenuItemsForMenu(this.name, this.allergens, this.typeCriteria, 
      this.availableCriteria, this.selectedCategory, this.selectedSortOption, this.sortAscOrDesc, 
      this.pIndex, this.pSize, this.inMenuCriteria, this.menuId).subscribe({
        next: (data: MenuItemSearchResults) => {
          this.displayedMenuItems = data.menuItemsInPage;
          this.totalCount = data.totalElements;
          console.log(data)
          return data;
        }
      });
  }

  search(){
    if (this.mode === 'All')
      this.fetchDataAll();
    else
      this.fetchDataMenu();
  }

  editMenuItem(menuItem: MenuItem) {
    this.router.navigate([`/manager/updatemenuitem`], { state: menuItem });
  }

  deleteMenuItem(id: number) {
    this.menuItemService.deleteMenuItem(id).subscribe({
      next: () => {
        this.toastr.success("Successfully deleted menu item from the system!");
        this.fetchDataAll();
      },
      error: err => {
        this.toastr.error("Unable to delete menu item!");
      }
    });
  }

  initForMenu(): void {    
    this.menuName = history.state.menuName;
    this.menuId = history.state.menuId;
    this.fetchDataMenu();
  }

  addToMenu(menuItemId: number) {
    const dialogRef = this.dialog.open(MenuItemPriceChangeDialog, {
      width: '250px',
      data: 1,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);

      if (result) {
        if (result < 0) {
          this.toastr.error("Invalid price!");
          return;
        }

        this.menuItemService.addMenuItemToMenu(this.menuId, menuItemId, result).subscribe({
          next: () => {
            this.toastr.success("Successfully added menu item to menu!");
            this.fetchDataMenu();
          },
          error: err => {
            this.toastr.error("Unable add menu item to menu!");
          }
        });
      }
    });
  }

  removeFromMenu(menuItemId: number) {
    this.menuItemService.removeMenuItemFromMenu(this.menuId, menuItemId).subscribe({
      next: () => {
        this.toastr.success("Successfully removed menu item from the menu!");
        this.fetchDataMenu();
      },
      error: err => {
        this.toastr.error("Unable to remove menu item from the menu!");
      }
    });
  }

  changePriceInMenu(menuItemId: number) {
    const dialogRef = this.dialog.open(MenuItemPriceChangeDialog, {
      width: '250px',
      data: 1,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);

      if (result) {
        if (result < 0) {
          this.toastr.error("Invalid price!");
          return;
        }

        this.menuItemService.updatePriceForMenuItemInMenu(this.menuId, menuItemId, result).subscribe({
          next: () => {
            this.toastr.success("Successfully updated the price!");
            this.fetchDataMenu();
          },
          error: err => {
            this.toastr.error("Unable to update the price!");
          }
        });
      }
    });
  }

}

@Component({
  selector: 'menu-item-price-change-dialog',
  templateUrl: 'menu-item-price-change-dialog.html',
})
export class MenuItemPriceChangeDialog {
  constructor(
    public dialogRef: MatDialogRef<MenuItemPriceChangeDialog>,
    @Inject(MAT_DIALOG_DATA) public data: number,
  ) {}

  onNoClick(): void {
    this.data = -1;
    this.dialogRef.close();
  }
}
