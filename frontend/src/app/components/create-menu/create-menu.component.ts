import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MenuService } from 'src/app/services/menu-service/menu.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-create-menu',
  templateUrl: './create-menu.component.html',
  styleUrls: ['./create-menu.component.css']
})
export class CreateMenuComponent implements OnInit {
  name = new FormControl('');

  createForm = new FormGroup({
    name: this.name
  });

  mode = 'Create';
  menuId = -1;

  constructor(
   private menuService: MenuService,
    private toastr: ToastrService,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit() {
    if (this.router.url.includes('update')) {
      this.mode = 'Update';
      this.initForUpdate();
    }
  }

  createMenu(): void {
    const formData = new FormData();

    formData.append('name', this.name.value);

    if (this.mode === 'Create') {
      this.menuService.createMenu(formData).subscribe({
        next: (result) => {
          this.toastr.success("Successfully added new menu to the system!");
          this.router.navigate(["/manager/menuview"]);
        },
        error: (error) => {
          this.toastr.error(error.error);
        }
      });
    }
    else {
      //update
      formData.append('id', history.state.id);

      this.menuService.updateMenu(history.state.id, this.name.value).subscribe({
        next: (result) => {
          this.toastr.success("Successfully updated menu!");
          this.location.back();
        },
        error: (error) => {
          this.toastr.error("Menu failed to update.");
        }
      });
    }
 
  }

  initForUpdate(): void {
    console.log(history.state);

    this.name.setValue(history.state.name);
  }

}

