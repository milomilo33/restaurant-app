import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { MenuItemService } from 'src/app/services/menu-item-service/menu-item.service';
import { ToastrService } from "ngx-toastr";
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import {Location} from '@angular/common';

@Component({
  selector: 'app-menu-item-create',
  templateUrl: './menu-item-create.component.html',
  styleUrls: ['./menu-item-create.component.css']
})
export class MenuItemCreateComponent implements OnInit {
  menuItemType = new FormControl('food');
  name = new FormControl('');
  suggestedPrice = new FormControl(0.0);
  allergens = new FormControl('');
  waitingTime = new FormControl(0.0);
  available = new FormControl('available');
  ingredients = new FormControl('');
  ingredientCost = new FormControl(0.0);

  category = new FormControl();
  categoryOptions: string[] = [];

  filteredCategoryOptions!: Observable<string[]>;

  selectedFiles!: FileList;
  selectedFileName = '';
  preview: string = '';
  fileControl = new FormControl('');

  createForm = new FormGroup({
    menuItemType: this.menuItemType,
    name: this.name,
    suggestedPrice: this.suggestedPrice,
    allergens: this.allergens,
    waitingTime: this.waitingTime,
    available: this.available,
    ingredients: this.ingredients,
    ingredientCost: this.ingredientCost,
    category: this.category,
    fileControl: this.fileControl
  });

  mode = 'Create';
  menuItemId = -1;

  constructor(
    private menuItemService: MenuItemService,
    private toastr: ToastrService,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit() {
    this.menuItemService.getMenuItemCategories().subscribe({
      next: (result) => {
        this.categoryOptions = result;
      },
      error: (error) => {
        this.toastr.error(error.error.error);
      },
      complete: () => {
        this.filteredCategoryOptions = this.category.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value)),
        );

        if (this.router.url.includes('update')) {
          this.mode = 'Update';
          this.initForUpdate();
        }
      }
    });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.categoryOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  
  selectFile(event: any): void {
    this.selectedFileName = '';
    this.selectedFiles = event.target.files;
    console.log(this.selectedFiles);
    console.log("xdd");

    if (this.selectedFiles && this.selectedFiles[0]) {
      const numberOfFiles = this.selectedFiles.length;
      for (let i = 0; i < numberOfFiles; i++) {
        const reader = new FileReader();

        reader.onload = (e: any) => {
          console.log(e.target.result);
          this.preview = e.target.result;
        };

        reader.readAsDataURL(this.selectedFiles[i]);

        this.selectedFileName = this.selectedFiles[i].name;
      }
    }
  }


  createMenuItem(): void {
    const formData = new FormData();

    formData.append('name', this.name.value);
    formData.append('ingredients', this.ingredients.value);
    formData.append('price', this.suggestedPrice.value);
    formData.append('allergens', this.allergens.value);
    formData.append('waitingTime', this.waitingTime.value);
    formData.append('category', this.category.value);
    formData.append('ingredientCost', this.ingredientCost.value);
    formData.append('available', this.available.value === 'available' ? 'true' : 'false');
    formData.append('image', this.preview.split(',')[1]);
    formData.append('food', this.menuItemType.value === 'food' ? 'true' : 'false');

    if (this.mode === 'Create') {
      this.menuItemService.createMenuItem(formData).subscribe({
        next: (result) => {
          this.toastr.success("Successfully added new menu item to the system!");
          this.router.navigate(["/manager/menuitemview"]);
        },
        error: (error) => {
          this.toastr.error(error.error);
        }
      });
    }
    else {
      //update
      formData.append('id', history.state.id);

      this.menuItemService.updateMenuItem(formData).subscribe({
        next: (result) => {
          this.toastr.success("Successfully updated menu item!");
          this.location.back();
        },
        error: (error) => {
          this.toastr.error("Menu item failed to update.");
        }
      });
    }
    // this.menuItemService.createMenuItem(formData).pipe(
    //   map(data => {
    //     this.toastr.success("Successfully added new menu item to the system!");
    //     this.router.navigate([""]);
    //   }),
    //   catchError(err => {
    //     this.toastr.error(err);
    //     console.log("error gets executed");
    //     return throwError(() => new Error(err));
    //   }));
    // // .subscribe({
    // //   next: (result) => {
    // //     this.toastr.success("Successfully added new menu item to the system!");
    // //     this.router.navigate([""]);
    // //   },
    // //   error: (error) => {
    // //     this.toastr.error(error.error);
    // //     console.log("error gets executed");
    // //   }
    // // });
  }

  initForUpdate(): void {
    console.log(history.state);
    
    this.allergens.setValue(history.state.allergens);
    this.available.setValue(history.state.available ? 'available' : 'unavailable');
    this.category.setValue(history.state.category);
    this.menuItemType.setValue(history.state.food ? 'food' : 'drink');
    this.preview = 'data:image/png;base64,' + history.state.image;
    this.ingredientCost.setValue(history.state.ingredientCost);
    this.ingredients.setValue(history.state.ingredients);
    this.name.setValue(history.state.name);
    this.suggestedPrice.setValue(history.state.price);
    this.waitingTime.setValue(history.state.waitingTime);
  }

}
