import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Menu } from 'src/app/models/menu';
import { MenuSearchResults } from 'src/app/models/menu-search';
import { MenuService } from 'src/app/services/menu-service/menu.service';

@Component({
  selector: 'app-menu-view',
  templateUrl: './menu-view.component.html',
  styleUrls: ['./menu-view.component.css']
})
export class MenuViewComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  totalCount: number = 0;
  pIndex: number = 0;
  pSize: number = 10;

  name = '';
  activeCriteria = 'Any';
  availableCriteria = 'Any';
  sortAscOrDesc = 'asc';
  displayedMenus: Menu[] = [];

  constructor(private http: HttpClient, private menuService: MenuService, private toastr: ToastrService, private router: Router) {
    this.fetchData();
  }

  ngOnInit(): void {
  }

  pageChanged(event: PageEvent) {
    this.pIndex = event?.pageIndex;
    this.pSize = event?.pageSize;
    console.log(event)
    this.fetchData();
  }

  fetchData() {
    this.menuService.searchSortFilterMenus(this.name, this.activeCriteria, this.sortAscOrDesc, this.pIndex, this.pSize).subscribe({
        next: (data: MenuSearchResults) => {
          this.displayedMenus = data.menusInPage;
          this.totalCount = data.totalElements;
          return data;
        }
      });
  }

  search() {
    this.fetchData();
  }

  editMenu(menu: Menu) {
    this.router.navigate([`/manager/updatemenu`], { state: menu });
  }

  deleteMenu(id: number) {
    this.menuService.deleteMenu(id).subscribe({
      next: () => {
        this.toastr.success("Successfully deleted menu from the system!");
        this.fetchData();
      },
      error: err => {
        this.toastr.error("Unable to delete menu!");
      }
    });
  }

  activateMenu(id: number) {
    this.menuService.activateMenu(id).subscribe({
      next: () => {
        this.toastr.success("Successfully activated menu!");
        this.fetchData();
      },
      error: err => {
        this.toastr.error("Unable to activate menu!");
      }
    });
  }

  manageMenuItems(menuId: number, menuName: string) {
    this.router.navigate([`/manager/menuitemsformenu`], { state: { menuId, menuName } });
  }

}
