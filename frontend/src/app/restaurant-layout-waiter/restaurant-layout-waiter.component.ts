import {Component, Inject, Input, NgModule, OnInit, ViewChild} from '@angular/core';
import {CdkDragEnd, CdkDragMove, CdkDragStart} from "@angular/cdk/drag-drop";
import {MatMenuTrigger} from "@angular/material/menu";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AddTableDialogue} from "../restaurant-layout/restaurant-layout.component";
import {ToastrService} from "ngx-toastr";

export interface DialogData {
  order: any
}

@Component({
  selector: 'app-restaurant-layout-waiter',
  templateUrl: './restaurant-layout-waiter.component.html',
  styleUrls: ['./restaurant-layout-waiter.component.css']
})
export class RestaurantLayoutWaiterComponent implements OnInit {

  private tableType: number | undefined;
  private smoking: boolean;

  //url; //Angular 8
  imgUrl: any; //Angular 11, for stricter type
  msg = "";
  tablePositions = [
    {dragPosition: {x: 0, y:0}, id: 0, tableType: 2, displayName: 1, smoking: false, occupied: false},
    {dragPosition: {x: 0, y: 10}, id: 1, tableType: 4, displayName: 2, smoking: false, occupied: false}
  ];

  constructor(public dialog: MatDialog, private http: HttpClient, private toastr: ToastrService) {
    this.smoking = false;
  }

  ngOnInit(): void {
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    this.http.get<any>('/api/layout/getRestaurantLayout',{
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    }).subscribe(
      data => {
        this.imgUrl = data.imgUrl;
        this.tablePositions = data.tablePositions;
        this.tablePositions.sort((a, b) => (a.id > b.id) ? 1 : -1);
      }
    )
  }

  //context menu
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger | undefined;
  contextMenuPosition = { x: '0px', y: '0px' };


  addOrder(tablePosition: any){
    const dialogRef = this.dialog.open(AddOrderDialogue, {
      width: '750px',
      height: '750px',
      data: {tableType: this.tableType, smoking: this.smoking},
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){let order = result.order;
      order.layoutTableId = tablePosition.id;

      let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
      this.http.post<any>('/api/orders/createOrder', order,{
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      }).subscribe(
        dataRet => {
          this.toastr.success('Order addition successful!');

        }
      )}
    });
  }

  writeReceipt(tablePosition: any){

  }

  onContextMenu(event: MouseEvent, tablePosition: any) {
    if(event && this.contextMenu) {
      event.preventDefault();
      this.contextMenuPosition.x = event.clientX + 'px';
      this.contextMenuPosition.y = event.clientY + 'px';
      this.contextMenu.menuData = {'tablePosition': tablePosition};
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }



}

@Component({
  selector: 'add-order-dialogue',
  templateUrl: 'add-order-dialogue.html',
  styleUrls: ['./restaurant-layout-waiter.component.css']
})
export class AddOrderDialogue {
  menu: any;
  displayedMenuItemColumns: string[] = ['category', 'name', 'price', 'waitingTime', 'add'];
  displayedOrderItemColumns: string[] = ['name', 'amount', 'note', 'remove'];
  displayedOrderItems: any;
  amount: number;
  noteInItem: string;

  constructor(
    public dialogRef: MatDialogRef<AddTableDialogue>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private http: HttpClient
  ) {
    this.noteInItem = "";
    this.amount = 1;
    this.displayedOrderItems = [];
    this.data.order = {
      layoutTableId: 0,
      orderItemDTOS: [],
      noteIn: ""
    };
    this.menu = [{
      category: "",
      food: false,
      menuItemId: 0,
      name: "",
      price: 0,
      waitingTime: 1
    }];

    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    this.http.get<any>('/api/menu/activeMenuOrder',{
          headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
        }
    ).subscribe({
      next: dataRet => {
        this.menu = dataRet;
        console.log(this.menu);

      },
      error: error => {

      }
    })
  }

  addMenuItemToOrder(menuItem: any){
    let id = 0;
    if(this.data.order.orderItemDTOS.length !== 0){
      id = Math.max.apply(Math, this.data.order.orderItemDTOS.map(function (item: any) {
        return item.orderItemId;
      }));
    }
    this.data.order.orderItemDTOS = [...this.data.order.orderItemDTOS, {
      menuItemId: menuItem.menuItemId,
      quantity: this.amount,
      noteIn: this.noteInItem,
      orderItemId: id + 1
    }];
    this.displayedOrderItems = [...this.displayedOrderItems, {
      menuItemId: menuItem.menuItemId,
      quantity: this.amount,
      noteIn: this.noteInItem,
      orderItemId: id + 1,
      name: menuItem.name
    }]
  }

  removeMenuItemFromOder(orderItem: any){
    this.data.order.orderItemDTOS = this.data.order.orderItemDTOS.filter(function(item: any) {
      return item.orderItemId !== orderItem.orderItemId
    });
    this.displayedOrderItems = this.displayedOrderItems.filter(function(item: any) {
      return item.orderItemId !== orderItem.orderItemId
    })
  }

  onChange(val: any){
    if(val < 0)
      this.amount = 1;
    if(typeof val != 'number'){
      this.amount = 1;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
