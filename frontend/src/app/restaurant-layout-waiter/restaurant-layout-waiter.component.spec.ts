import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantLayoutWaiterComponent } from './restaurant-layout-waiter.component';

describe('RestaurantLayoutWaiterComponent', () => {
  let component: RestaurantLayoutWaiterComponent;
  let fixture: ComponentFixture<RestaurantLayoutWaiterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestaurantLayoutWaiterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantLayoutWaiterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
