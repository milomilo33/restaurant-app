import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {

  userDetails: any;

  constructor(private http: HttpClient) {
    if(localStorage.getItem("user")) {
      let username: String = JSON.parse(
        atob(localStorage.getItem("user")!.split(".")[1])
      ).username;
      console.log(JSON.parse(
        atob(localStorage.getItem("user")!.split(".")[1])
      ).username);

      let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
      this.http.get<any>('/api/userDetails?username=' + username, {
          headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
        }
      ).subscribe(
        res => {
          this.userDetails = res;
          this.userDetails.wage += ' RSD'
        }
      );
    }
  }

  ngOnInit(): void {
  }

}
