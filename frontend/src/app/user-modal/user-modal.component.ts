import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {EmployeesTableItem} from "../employees-table/employees-table-datasource";
import {ManagerServiceService} from "../services/manager/manager-service.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.css']
})
export class UserModalComponent implements OnInit {

  user: EmployeesTableItem = {
    emailAddress: "", firstName: "", lastName: "", phoneNumber: "", type: "", username: "", wage: 0
  };

  errorMessage: string | undefined;
  newUser: boolean = false;
  username: string | undefined;

  userForm = new FormGroup(
    {
      emailAddress: new FormControl("",
        [Validators.required, Validators.email]),
      firstName: new FormControl("",
        [Validators.minLength(1), Validators.maxLength(30)]),
      lastName: new FormControl("",
        [Validators.minLength(1), Validators.maxLength(30)]),
      phoneNumber: new FormControl("",
        [Validators.required]),
      type: new FormControl({value: ""}),
      username: new FormControl({value: ""}),
      wage: new FormControl(0, Validators.min(0)),

    }
  )

  roles: string[] = [
    "HEAD CHEF", "BARMAN", "WAITER", "CHEF"
  ]


  constructor(
    public dialogRef: MatDialogRef<UserModalComponent>,
    private _managerService: ManagerServiceService,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: [string, boolean]) {
  }

  onNoClick(): void {


    if (!this.userForm.valid) {
      this.errorMessage = "Provide correct input"
      return;
    }


    this.user.username = this.userForm.get('username')?.value;
    this.user.firstName = this.userForm.get('firstName')?.value;
    this.user.lastName = this.userForm.get('lastName')?.value;
    this.user.phoneNumber = this.userForm.get('phoneNumber')?.value;
    this.user.type = this.userForm.get('type')?.value;
    this.user.wage = this.userForm.get('wage')?.value;
    this.user.emailAddress = this.userForm.get('emailAddress')?.value;

    this._managerService.putEmployee(this.user).subscribe(
      (res) => {

        this._managerService.getEmployees();
        this.dialogRef.close({
          user: res
        });
      }
    );

  }

  ngOnInit(): void {

    console.log(this.data)

    this.newUser = this.data[0] === ""

    this.username = this.data[0]

    if (!this.data[1]) {
      this.roles = ['MANAGER', ...this.roles]
    }

    if (this.route.snapshot.data['user'] === 'manager') {
      this._managerService.baseRoute = 'api/staff'

    } else {
      this._managerService.baseRoute = 'api/employee'
    }

    if (this.newUser) return;

    this._managerService.getEmployee(this.data[0]).subscribe(
      (result: EmployeesTableItem) => {
        this.userForm.setValue(result)
        this.user.type = this.userForm.get('type')?.value
        console.log(this.user);
        return result;
      }
    )
  }

  addNewUser() {
    if (this.username != null) {
      this.user.username = this.username;
    }
    this.user.firstName = this.userForm.get('firstName')?.value;
    this.user.lastName = this.userForm.get('lastName')?.value;
    this.user.phoneNumber = this.userForm.get('phoneNumber')?.value;
    this.user.type = this.userForm.get('type')?.value;
    this.user.wage = this.userForm.get('wage')?.value;
    this.user.emailAddress = this.userForm.get('emailAddress')?.value;


    if (!this.userForm.valid) return;


    this.dialogRef.close();

    this._managerService.postEmployee(this.user).subscribe(
      () => {
      }
    )
  }
}
