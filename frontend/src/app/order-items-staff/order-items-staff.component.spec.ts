import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderItemsStaffComponent } from './order-items-staff.component';

describe('OrderItemsStaffComponent', () => {
  let component: OrderItemsStaffComponent;
  let fixture: ComponentFixture<OrderItemsStaffComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderItemsStaffComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderItemsStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
