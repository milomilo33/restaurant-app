import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-order-items-staff',
  templateUrl: './order-items-staff.component.html',
  styleUrls: ['./order-items-staff.component.css']
})
export class OrderItemsStaffComponent implements OnInit {

  currentPage: number;
  totalPages: number;
  resultsPerPage: number;
  userRole: string;
  criteria: string;

  orderItems: any;
  filteredOrderItems: any;
  displayedOrderItems: any;

  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.currentPage = 1;
    this.resultsPerPage = 10;
    this.totalPages = 1;
    this.displayedOrderItems = [];
    this.orderItems = [];
    this.filteredOrderItems = [];
    this.userRole = "ROLE_CHEF";
    this.criteria = "Any";
    this.initOrderItems();
    this.initDisplayedOrderItems();
  }

  initDisplayedOrderItems(){
    this.totalPages = Math.ceil(this.filteredOrderItems.length / this.resultsPerPage);
    if(this.totalPages <= 0)
      this.totalPages = 1;
    this.displayedOrderItems = this.filteredOrderItems.slice((this.currentPage - 1)*this.resultsPerPage, this.currentPage*this.resultsPerPage)
  }

  initOrderItems(){
    this.userRole = JSON.parse(
      atob(localStorage.getItem("user")!.split(".")[1])
    ).role;
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    this.http.get<any>('/api/orderItems/getOrderItems?role=' + this.userRole,  {
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    }).subscribe(
      data => {
        this.orderItems = data;
        this.totalPages = Math.ceil(this.orderItems.length / this.resultsPerPage);
        if(this.totalPages <= 0)
          this.totalPages = 1;
        this.orderItems.sort((a: any, b: any) => (b.id > a.id) ? 1 : -1);
        this.onChange(this.criteria);
      },
      error => { }
    )
  }

  nextPage(){
    this.currentPage += 1;
    if(this.currentPage > this.totalPages)
      this.currentPage = this.totalPages;
    else
      this.initDisplayedOrderItems()
  }

  prevPage(){
    this.currentPage -= 1;
    if(this.currentPage < 1)
      this.currentPage = 1;
    else
      this.initDisplayedOrderItems()
  }

  ngOnInit(): void {
  }

  //---------------------------------------------------------------------

  doOrderItem(type: string, id: any, msgToaster: string){
    let token: string = JSON.parse(localStorage.getItem('user')!)["accessToken"];
    this.http.post<any>('/api/orderItems/'+type+'OrderItem?id=' + id,{}, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
    } ).subscribe(
      data => {
        this.initOrderItems();
        this.toastr.success("Order item has been " + msgToaster + '!')
      }
    )
  }

  acceptOrderItem(id: any){
    this.doOrderItem('accept', id, 'accepted');
  }

  readyOrderItem(id: any){
    this.doOrderItem('ready', id, 'readied');
  }

  receiveOrderItem(id: any){
    this.doOrderItem('receive', id, 'received');
  }

  onChange(selectedValue: any){
    this.criteria = selectedValue;
    this.filteredOrderItems = this.orderItems;
    this.filteredOrderItems = this.filteredOrderItems.filter(function (orderItem: any) {
      if(selectedValue ===  "Any")
        return true;
      let tmp = orderItem.orderStatus;
        //return true;
      return tmp === selectedValue;
    });
    this.initDisplayedOrderItems();
  }
}
