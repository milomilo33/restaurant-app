import {Component, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {WebSocketShareService} from "../services/sockets/web-socket-share.service";
import {SocketService} from "../services/sockets/socket.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userRole: any;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private toastr: ToastrService, private websocketService: WebSocketShareService, private webSocketAPI: SocketService, private breakpointObserver: BreakpointObserver, private router: Router) {
    this.initRole();
  }


  signOut() {

    // @ts-ignore
    localStorage.removeItem("user");
    this.router.navigate(['/login']);
    this.initRole();
  }

  initRole() {
    this.userRole = "ALL";
    if (localStorage.getItem("user"))
      this.userRole = JSON.parse(
        atob(localStorage.getItem("user")!.split(".")[1])
      ).role;
  }


  ngOnInit() {
    this.webSocketAPI._connect();
    this.onNewValueReceive();
    this.router.events.subscribe(() => this.initRole());
  }

  ngOnDestroy() {

  }


  connect() {
    this.webSocketAPI._connect();
  }

  disconnect() {
    this.webSocketAPI._disconnect();
  }

  // method to receive the updated data.
  onNewValueReceive() {
    this.websocketService.getNewValue().subscribe(resp => {
      if (resp !== undefined) {
        if (localStorage.getItem("user")) {
          this.userRole = JSON.parse(
            atob(localStorage.getItem("user")!.split(".")[1])
          ).role;
          if (resp.split(':')[0] == this.userRole)
            this.toastr.success(resp.split(':')[1]);
        }
      }
    });
  }

}
