import { MenuItem } from "./menu-item";

export interface MenuItemSearchResults {
    pageNumber: number;
    pageNumberOfElements: number;
    totalElements: number;
    totalPages: number;
    menuItemsInPage: MenuItem[];
}