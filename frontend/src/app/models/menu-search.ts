import { Menu } from "./menu";

export interface MenuSearchResults {
    pageNumber: number;
    pageNumberOfElements: number;
    totalElements: number;
    totalPages: number;
    menusInPage: Menu[];
}