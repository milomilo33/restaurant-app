export interface Menu {
    id: number;
    name: string;
    active: boolean;
}