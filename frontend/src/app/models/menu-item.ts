export interface MenuItem {
    id: number;
    name: string;
    ingredients: string;
    price: number;
    allergens: string;
    waitingTime: number;
    category: string;
    ingredientCost: number;
    available: boolean;
    image: string;
    suggestion: boolean;
    food: boolean;
    deleted: boolean;
    priceInMenu: number;
    menuId: number;
}