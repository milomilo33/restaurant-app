import {Component, OnInit} from '@angular/core';
import {ReportService} from "../services/report/report.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-manager-reports',
  templateUrl: './manager-reports.component.html',
  styleUrls: ['./manager-reports.component.css']
})
export class ManagerReportsComponent implements OnInit {

  constructor(private reportService: ReportService) {
  }

  data: [] = [];
  rangeRole = new FormGroup({
    start: new FormControl(new Date((new Date()).setFullYear((new Date()).getFullYear() - 1))),
    end: new FormControl(new Date()),
  });

  rangeUsername = new FormGroup({
    start: new FormControl(new Date((new Date()).setFullYear((new Date()).getFullYear() - 1))),
    end: new FormControl(new Date()),
  });

  rangeMenuItem = new FormGroup({
    start: new FormControl(new Date((new Date()).setFullYear((new Date()).getFullYear() - 1))),
    end: new FormControl(new Date()),
  });

  rangeMenuItemPrepare = new FormGroup({
    start: new FormControl(new Date((new Date()).setFullYear((new Date()).getFullYear() - 1))),
    end: new FormControl(new Date()),
  });

  rangeMenuItemPrice = new FormGroup({
    start: new FormControl(new Date((new Date()).setFullYear((new Date()).getFullYear() - 1))),
    end: new FormControl(new Date()),
  });

  role: String = "barman"
  username: String = JSON.parse(
    atob(localStorage.getItem("user")!.split(".")[1])
  ).username;

  selected: any;
  dataUsername: [] = [];

  dataMenuItem: [] = [];

  dataMenuItemPrepare: [] = [];

  dataMenuItemPrice: [] = [];

  roles = ['barman', 'waiter', 'manager']

  menuItems: String[] = [];

  menuItem: string | undefined;

  menuItemPrepare: string | undefined;

  menuItemPrice: string | undefined;

  menuItemReportDisplay: boolean = false;
  menuItemReportDisplayPrice: boolean = false;
  menuItemReportDisplayPrepare: boolean = false;
  usernameReportDisplay: boolean = false;
  roleReportDisplay: boolean = false;

  view: any[] = [1000, 500];
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Average Wage For Given Role In A Day';

  xAxisLabel1 = 'Employee Wage In A Day';
  showYAxisLabel = true;
  yAxisLabel = 'Wage';
  timeline = true;
  doughnut = true;

  xAxisLabelMenu = 'Profit In A Day';
  yAxisLabelMenu = 'Value';

  xAxisLabelMenu1 = 'Prepare Cost In A Day';

  xAxisLabelMenu2 = 'Total Price Sold In A Day';


  ngOnInit(): void {
    let start_in_date = (new Date())
    start_in_date.setFullYear((new Date()).getFullYear() - 1)
    this.reportService.getDataRole(this.role, start_in_date, new Date()).subscribe((r) => this.data = r)
    this.reportService.getDataUsername(this.username, start_in_date, new Date()).subscribe((r) => this.dataUsername = r)
    this.reportService.getMenuItemNames().subscribe((r) => {
      this.menuItems = r;
      this.menuItem = r[0];
      this.menuItemPrepare = r[0];
      this.menuItemPrice = r[0];
      this.reportService.getDataMenuItem(this.menuItem, start_in_date, new Date()).subscribe((r) => this.dataMenuItem = r)
      this.reportService.getDataMenuItemPrice(this.menuItem, start_in_date, new Date()).subscribe((r) => this.dataMenuItemPrice = r)
      this.reportService.getDataMenuItemPrepare(this.menuItem, start_in_date, new Date()).subscribe((r) => this.dataMenuItemPrepare = r)

      return r;
    });


  }


  generateReport() {
    this.reportService.getDataRole(this.role, this.rangeRole.get('start')?.value, this.rangeRole.get('end')?.value).subscribe((r) => this.data = r)
  }

  generateReportUsername() {
    this.reportService.getDataUsername(this.username, this.rangeUsername.get('start')?.value, this.rangeUsername.get('end')?.value).subscribe((r) => this.dataUsername = r)
  }

  generateReportMenuItem() {
    this.reportService.getDataMenuItem(this.menuItem, this.rangeMenuItem.get('start')?.value, this.rangeMenuItem.get('end')?.value).subscribe((r) => this.dataMenuItem = r)
  }

  onChange(event: String[]) {
    this.roleReportDisplay = event.includes('Role');
    this.menuItemReportDisplay = event.includes('MenuItem');
    this.usernameReportDisplay = event.includes('User');
    this.menuItemReportDisplayPrepare = event.includes('MenuItemPrepare')
    this.menuItemReportDisplayPrice = event.includes('MenuItemPrice')
  }

  generateReportMenuItemPrepare() {
    this.reportService.getDataMenuItemPrepare(this.menuItem, this.rangeMenuItem.get('start')?.value, this.rangeMenuItem.get('end')?.value).subscribe((r) => this.dataMenuItemPrepare = r)

  }

  generateReportMenuItemPrice() {
    this.reportService.getDataMenuItemPrice(this.menuItem, this.rangeMenuItemPrice.get('start')?.value, this.rangeMenuItemPrice.get('end')?.value).subscribe((r) => this.dataMenuItemPrice = r)

  }
}
