package com.ktsnwt.restaurantapp.controller;

import com.ktsnwt.restaurantapp.exception.OrderItemNotFoundException;
import com.ktsnwt.restaurantapp.service.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class OrderItemController {
    @Autowired
    private OrderItemService orderItemService;

    @RequestMapping(value = "/api/orderItems/cancelOrderItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('WAITER')")
    public ResponseEntity<?> cancelOrderItem(@RequestParam("id") Long orderItemId){
        try {
            return ResponseEntity.ok().body(orderItemService.cancelOrderItem(orderItemId));
        }catch (OrderItemNotFoundException ex){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Provide correct Order item Id", ex);
        }
    }

    @RequestMapping(value = "/api/orderItems/serveOrderItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('WAITER')")
    public ResponseEntity<?> serveOrderItem(@RequestParam("id") Long orderItemId){
        try {
            return ResponseEntity.ok().body(orderItemService.serveOrderItem(orderItemId));
        }catch (OrderItemNotFoundException ex){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Provide correct Order item Id", ex);
        }
    }

    @RequestMapping(value = "/api/orderItems/denyOrderItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('WAITER')")
    public ResponseEntity<?> denyOrderItem(@RequestParam("id") Long orderItemId){
        try {
            return ResponseEntity.ok().body(orderItemService.denyOrderItem(orderItemId));
        }catch (OrderItemNotFoundException ex){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Provide correct Order item Id", ex);
        }
    }

    @RequestMapping(value = "/api/orderItems/chargeOrderItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('WAITER')")
    public ResponseEntity<?> chargeOrderItem(@RequestParam("id") Long orderItemId){
        try {
            return ResponseEntity.ok().body(orderItemService.chargeOrderItem(orderItemId));
        }catch (OrderItemNotFoundException ex){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Provide correct Order item Id", ex);
        }
    }

    @RequestMapping(value = "/api/orderItems/acceptOrderItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('WAITER') || hasRole('CHEF') || hasRole('BARMAN')")
    public ResponseEntity<?> acceptOrderItem(@RequestParam("id") Long orderItemId){
        try {
            orderItemService.acceptOrderItem(orderItemId);
            return ResponseEntity.ok().body("{}");
        }catch (OrderItemNotFoundException ex){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Provide correct Order item Id", ex);
        }
    }

    @RequestMapping(value = "/api/orderItems/readyOrderItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('WAITER') || hasRole('CHEF') || hasRole('BARMAN')")
    public ResponseEntity<?> readyOrderItem(@RequestParam("id") Long orderItemId){
        try {
            orderItemService.readyOrderItem(orderItemId);
            return ResponseEntity.ok().body("{}");
        }catch (OrderItemNotFoundException ex){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Provide correct Order item Id", ex);
        }
    }

    @RequestMapping(value = "/api/orderItems/receiveOrderItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('WAITER') || hasRole('CHEF') || hasRole('BARMAN')")
    public ResponseEntity<?> receiveOrderItem(@RequestParam("id") Long orderItemId){
        try {
            orderItemService.receiveOrderItem(orderItemId);
            return ResponseEntity.ok().body("{}");
        }catch (OrderItemNotFoundException ex){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Provide correct Order item Id", ex);
        }
    }

    @RequestMapping(value = "/api/orderItems/getOrderItems", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('WAITER') || hasRole('CHEF') || hasRole('BARMAN')")
    public ResponseEntity<?> getOrderItems(@RequestParam("role") String role){
        return ResponseEntity.ok().body(orderItemService.getOrderItems(role));
    }
}
