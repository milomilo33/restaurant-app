package com.ktsnwt.restaurantapp.controller;

import com.ktsnwt.restaurantapp.exception.InvalidReportTimeIntervalException;
import com.ktsnwt.restaurantapp.service.LogService;
import com.ktsnwt.restaurantapp.service.LogWageService;
import com.ktsnwt.restaurantapp.service.SocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/reports")
public class ReportsController {

    @Autowired
    private LogWageService logWageService;

    @Autowired
    private LogService logService;



    @RequestMapping(
            method = RequestMethod.GET,
            value = "/employee",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("hasAnyRole('ROLE_MANAGER')")
    public ResponseEntity<?> getEmployeeWageTimeInterval
            (
                    @RequestParam(value = "username") String username,
                    @RequestParam(value = "start_date", defaultValue = "2021-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                    @RequestParam(value = "end_date", defaultValue = "2022-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
            ) {
        //socketService.processMsg("I AM HERE");
        try {
            return ResponseEntity.ok().body(logWageService.getEmployeeWageTimeInterval(username, startDate, endDate));
        } catch (InvalidReportTimeIntervalException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/role",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("hasAnyRole('ROLE_MANAGER')")
    public ResponseEntity<?> getRoleWageTimeInterval
            (
                    @RequestParam(value = "role", defaultValue = "barman") String role,
                    @RequestParam(value = "start_date", defaultValue = "2021-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                    @RequestParam(value = "end_date", defaultValue = "2022-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
            ) {
        try {
            return ResponseEntity.ok().body(logWageService.getAverageRoleWageTimeInterval(role, startDate, endDate));
        } catch (InvalidReportTimeIntervalException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/menu-item-names",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("hasAnyRole('ROLE_MANAGER')")
    public ResponseEntity<?> getMenuItemNames() {
        return ResponseEntity.ok().body(logService.getOrderedItemNames());
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/menu-item",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("hasAnyRole('ROLE_MANAGER')")
    public ResponseEntity<?> getReportForMenuItem(@RequestParam(value = "name", required = true) String name,
                                                  @RequestParam(value = "start_date", defaultValue = "2021-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                  @RequestParam(value = "end_date", defaultValue = "2022-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate

    ) {
        try {
            return ResponseEntity.ok().body(logService.getSoldInTimePeriodForItem(name, startDate, endDate));
        } catch (InvalidReportTimeIntervalException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/menu-item-prepare",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("hasAnyRole('ROLE_MANAGER')")
    public ResponseEntity<?> getReportForMenuItemPrepare(@RequestParam(value = "name", required = true) String name,
                                                  @RequestParam(value = "start_date", defaultValue = "2021-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                  @RequestParam(value = "end_date", defaultValue = "2022-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate

    ) {
        try {
            return ResponseEntity.ok().body(logService.getSoldInTimePeriodForItemPrepare(name, startDate, endDate));
        } catch (InvalidReportTimeIntervalException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/menu-item-price",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("hasAnyRole('ROLE_MANAGER')")
    public ResponseEntity<?> getReportForMenuItemPrice(@RequestParam(value = "name", required = true) String name,
                                                         @RequestParam(value = "start_date", defaultValue = "2021-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                         @RequestParam(value = "end_date", defaultValue = "2022-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate

    ) {
        try {
            return ResponseEntity.ok().body(logService.getSoldInTimePeriodForItemPrice(name, startDate, endDate));
        } catch (InvalidReportTimeIntervalException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

}
