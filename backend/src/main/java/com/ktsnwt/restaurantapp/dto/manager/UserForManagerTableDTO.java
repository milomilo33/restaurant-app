package com.ktsnwt.restaurantapp.dto.manager;

import com.ktsnwt.restaurantapp.model.Role;
import com.ktsnwt.restaurantapp.model.User;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;


public class UserForManagerTableDTO {

    @Length(max = 30, min = 3)
    public String firstName;
    @Length(max = 30, min = 3)
    public String lastName;
    @Email
    public String emailAddress;
    public String username;
    public String type;
    @Min(value = 0)
    public Double wage;
    public String phoneNumber;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getWage() {
        return wage;
    }

    public void setWage(Double wage) {
        this.wage = wage;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public UserForManagerTableDTO() {
    }

    public UserForManagerTableDTO(User user) {
        this.firstName=user.getName();
        this.lastName=user.getSurname();
        this.emailAddress=user.getMail();
        this.username=user.getUsername();
        this.type= String.valueOf((user.getRoles().stream().findFirst().orElse(new Role())).getName()).split("_")[1];
        this.wage=user.getWage();
        this.phoneNumber=user.getPhoneNumber();
    }
}
