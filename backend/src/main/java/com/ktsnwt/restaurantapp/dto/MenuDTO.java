package com.ktsnwt.restaurantapp.dto;

import com.ktsnwt.restaurantapp.model.Menu;

public class MenuDTO {
    private String name;
    private Long id;
    private boolean active;

    public MenuDTO(String name, Long id, boolean active) {
        this.name = name;
        this.id = id;
        this.active = active;
    }

    public MenuDTO(Menu m) {
        this.name = m.getName();
        this.id = m.getId();
        this.active = m.isActive();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
