package com.ktsnwt.restaurantapp.dto.reports;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class WageDateDTO {
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate name;
    private Double value;

    public WageDateDTO() {
    }

    public WageDateDTO(Double value, LocalDate date) {
        this.value = value;
        this.name = date;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public LocalDate getName() {
        return name;
    }

    public void setName(LocalDate name) {
        this.name = name;
    }
}
