package com.ktsnwt.restaurantapp.dto;

import java.util.List;

public class MenuItemSearchSortFilterForMenuDTO {
    private int pageNumber;
    private int pageNumberOfElements;
    private long totalElements;
    private int totalPages;
    private List<MenuItemInMenuDTO> menuItemsInPage;

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageNumberOfElements() {
        return pageNumberOfElements;
    }

    public void setPageNumberOfElements(int pageNumberOfElements) {
        this.pageNumberOfElements = pageNumberOfElements;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<MenuItemInMenuDTO> getMenuItemsInPage() {
        return menuItemsInPage;
    }

    public void setMenuItemsInPage(List<MenuItemInMenuDTO> menuItemsInPage) {
        this.menuItemsInPage = menuItemsInPage;
    }

    public MenuItemSearchSortFilterForMenuDTO(int pageNumber, int pageNumberOfElements, long totalElements, int totalPages, List<MenuItemInMenuDTO> menuItemsInPage) {
        this.pageNumber = pageNumber;
        this.pageNumberOfElements = pageNumberOfElements;
        this.totalElements = totalElements;
        this.totalPages = totalPages;
        this.menuItemsInPage = menuItemsInPage;
    }
}
