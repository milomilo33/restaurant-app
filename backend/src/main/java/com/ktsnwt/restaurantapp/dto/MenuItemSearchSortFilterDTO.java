package com.ktsnwt.restaurantapp.dto;

import com.ktsnwt.restaurantapp.model.MenuItem;

import java.util.List;

public class MenuItemSearchSortFilterDTO {
    private int pageNumber;
    private int pageNumberOfElements;
    private long totalElements;
    private int totalPages;
    private List<MenuItemDTO> menuItemsInPage;

    public int getPageNumber() {
        return pageNumber;
    }

    public MenuItemSearchSortFilterDTO(int pageNumber, int pageNumberOfElements, long totalElements, int totalPages, List<MenuItemDTO> menuItemsInPage) {
        this.pageNumber = pageNumber;
        this.pageNumberOfElements = pageNumberOfElements;
        this.totalElements = totalElements;
        this.totalPages = totalPages;
        this.menuItemsInPage = menuItemsInPage;
    }

    public MenuItemSearchSortFilterDTO() {}

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageNumberOfElements() {
        return pageNumberOfElements;
    }

    public void setPageNumberOfElements(int pageNumberOfElements) {
        this.pageNumberOfElements = pageNumberOfElements;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<MenuItemDTO> getMenuItemsInPage() {
        return menuItemsInPage;
    }

    public void setMenuItemsInPage(List<MenuItemDTO> menuItemsInPage) {
        this.menuItemsInPage = menuItemsInPage;
    }
}
