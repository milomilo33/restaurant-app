package com.ktsnwt.restaurantapp.dto;

public class MenuItemForOrderDTO {
    private long menuItemId;
    private String name;
    private Double price;
    private Double waitingTime;
    private String category;
    private boolean food;

    public long getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(long menuItemId) {
        this.menuItemId = menuItemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(Double waitingTime) {
        this.waitingTime = waitingTime;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isFood() {
        return food;
    }

    public void setFood(boolean food) {
        this.food = food;
    }

    public MenuItemForOrderDTO(long menuItemId, String name, Double price, Double waitingTime, String category, boolean food) {
        this.menuItemId = menuItemId;
        this.name = name;
        this.price = price;
        this.waitingTime = waitingTime;
        this.category = category;
        this.food = food;
    }

    public MenuItemForOrderDTO() {
    }
}
