package com.ktsnwt.restaurantapp.dto;

import java.util.List;

public class MenuSearchSortFilterDTO {
    private int pageNumber;
    private int pageNumberOfElements;
    private long totalElements;
    private int totalPages;
    private List<MenuDTO> menusInPage;

    public int getPageNumber() {
        return pageNumber;
    }

    public MenuSearchSortFilterDTO(int pageNumber, int pageNumberOfElements, long totalElements, int totalPages, List<MenuDTO> menusInPage) {
        this.pageNumber = pageNumber;
        this.pageNumberOfElements = pageNumberOfElements;
        this.totalElements = totalElements;
        this.totalPages = totalPages;
        this.menusInPage = menusInPage;
    }

    public MenuSearchSortFilterDTO() {}

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageNumberOfElements() {
        return pageNumberOfElements;
    }

    public void setPageNumberOfElements(int pageNumberOfElements) {
        this.pageNumberOfElements = pageNumberOfElements;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<MenuDTO> getMenusInPage() {
        return menusInPage;
    }

    public void setMenusInPage(List<MenuDTO> menusInPage) {
        this.menusInPage = menusInPage;
    }
}
