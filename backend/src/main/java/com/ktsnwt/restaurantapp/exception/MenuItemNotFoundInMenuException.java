package com.ktsnwt.restaurantapp.exception;

public class MenuItemNotFoundInMenuException extends Exception {
    public MenuItemNotFoundInMenuException(String error) {
        super(error);
    }
}
