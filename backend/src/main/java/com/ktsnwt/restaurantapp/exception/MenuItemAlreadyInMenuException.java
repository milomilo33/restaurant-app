package com.ktsnwt.restaurantapp.exception;

public class MenuItemAlreadyInMenuException extends Exception {
    public MenuItemAlreadyInMenuException(String error) {
        super(error);
    }
}
