package com.ktsnwt.restaurantapp.exception;

public class InvalidSearchSortFilterParamsException  extends Exception {
    public InvalidSearchSortFilterParamsException(String message) {
        super(message);
    }
}
