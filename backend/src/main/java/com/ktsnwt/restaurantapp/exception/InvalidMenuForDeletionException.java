package com.ktsnwt.restaurantapp.exception;

public class InvalidMenuForDeletionException extends Exception {
    public InvalidMenuForDeletionException(String message) {
        super(message);
    }
}
