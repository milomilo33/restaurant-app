package com.ktsnwt.restaurantapp.exception;

public class InvalidUserSearchCriteriaException extends  Exception{
    public InvalidUserSearchCriteriaException(String message) {
        super(message);
    }
}
