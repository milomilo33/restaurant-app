package com.ktsnwt.restaurantapp.exception;

public class InvalidReportTimeIntervalException extends Exception {
    public InvalidReportTimeIntervalException(String message) {
        super(message);
    }
}