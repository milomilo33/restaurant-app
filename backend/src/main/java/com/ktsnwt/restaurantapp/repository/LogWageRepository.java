package com.ktsnwt.restaurantapp.repository;

import com.ktsnwt.restaurantapp.model.LogWage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogWageRepository extends JpaRepository<LogWage, Long> {
//    List<LogWage> findByEmployee_UsernameContainsIgnoreCaseOrderByTimestamp(String username);
//
//    List<LogWage> findByEmployee_RoleContainsIgnoreCaseOrderByTimestamp(String role);

    List<LogWage> findByOrderByTimestampAsc();





}
