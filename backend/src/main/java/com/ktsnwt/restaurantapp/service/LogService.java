package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.dto.reports.WageDateDTO;
import com.ktsnwt.restaurantapp.exception.InvalidReportTimeIntervalException;

import java.time.LocalDate;
import java.util.List;

public interface LogService {

    List<String> getOrderedItemNames();

    List<WageDateDTO> getSoldInTimePeriodForItem(String role, LocalDate startDate, LocalDate endDate) throws InvalidReportTimeIntervalException;

    List<WageDateDTO> getSoldInTimePeriodForItemPrice(String role, LocalDate startDate, LocalDate endDate) throws InvalidReportTimeIntervalException;

    List<WageDateDTO> getSoldInTimePeriodForItemPrepare(String role, LocalDate startDate, LocalDate endDate) throws InvalidReportTimeIntervalException;
}
