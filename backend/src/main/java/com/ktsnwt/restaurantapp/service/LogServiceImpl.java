package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.dto.reports.WageDateDTO;
import com.ktsnwt.restaurantapp.exception.InvalidReportTimeIntervalException;
import com.ktsnwt.restaurantapp.model.Log;
import com.ktsnwt.restaurantapp.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class LogServiceImpl  implements  LogService{

    @Autowired
    private LogRepository logRepository;

    @Override
    public List<String> getOrderedItemNames() {
        return logRepository.findNames();
    }


    @Override
    public List<WageDateDTO> getSoldInTimePeriodForItem(String menuItemName, LocalDate startDate, LocalDate endDate) throws InvalidReportTimeIntervalException {

        if(startDate.isAfter(endDate)) throw  new InvalidReportTimeIntervalException("Start date after end date ! ! ! ! !");

        List<Log> logs = logRepository.findByMenuItemNameContainsIgnoreCaseAndTimestampGreaterThanEqualAndTimestampLessThanEqualOrderByTimestampAsc(
                menuItemName, LocalDateTime.of(startDate, LocalTime.MIDNIGHT), LocalDateTime.of(endDate, LocalTime.MIDNIGHT));

        List<WageDateDTO> data = new ArrayList<>();

        while(!startDate.equals(endDate)){
            data.add(new WageDateDTO(getValueForDate(startDate, logs,""), startDate));
            startDate = startDate.plusDays(1);
        }
        return data;
    }

    @Override
    public List<WageDateDTO> getSoldInTimePeriodForItemPrice(String menuItemName, LocalDate startDate, LocalDate endDate) throws InvalidReportTimeIntervalException {

        if(startDate.isAfter(endDate)) throw  new InvalidReportTimeIntervalException("Start date after end date ! ! ! ! !");

        List<Log> logs = logRepository.findByMenuItemNameContainsIgnoreCaseAndTimestampGreaterThanEqualAndTimestampLessThanEqualOrderByTimestampAsc(
                menuItemName, LocalDateTime.of(startDate, LocalTime.MIDNIGHT), LocalDateTime.of(endDate, LocalTime.MIDNIGHT));

        List<WageDateDTO> data = new ArrayList<>();

        while(!startDate.equals(endDate)){
            data.add(new WageDateDTO(getValueForDate(startDate, logs, "price"), startDate));
            startDate = startDate.plusDays(1);
        }
        return data;
    }

    @Override
    public List<WageDateDTO> getSoldInTimePeriodForItemPrepare(String menuItemName, LocalDate startDate, LocalDate endDate) throws InvalidReportTimeIntervalException {

        if(startDate.isAfter(endDate)) throw  new InvalidReportTimeIntervalException("Start date after end date ! ! ! ! !");
        List<Log> logs = logRepository.findByMenuItemNameContainsIgnoreCaseAndTimestampGreaterThanEqualAndTimestampLessThanEqualOrderByTimestampAsc(
                menuItemName, LocalDateTime.of(startDate, LocalTime.MIDNIGHT), LocalDateTime.of(endDate, LocalTime.MIDNIGHT));

        List<WageDateDTO> data = new ArrayList<>();

        while(!startDate.equals(endDate)){
            data.add(new WageDateDTO(getValueForDate(startDate, logs, "prepare"), startDate));
            startDate = startDate.plusDays(1);
        }
        return data;
    }

    private Double getValueForDate(LocalDate startDate, List<Log> logs, String action) {
        return logs.stream()
                .filter(log -> log.getTimestamp().toLocalDate().equals(startDate))
                .mapToDouble(log -> {
                            if(action.equals("prepare")) return  log.getQuantity()*log.getIngredientCost();
                            if(action.equals("price")) return log.getPrice()*log.getPrice();
                            return log.getQuantity() * (log.getPrice() - log.getIngredientCost());
                        }
                )
                .average()
                .orElse(0);

    }

}
