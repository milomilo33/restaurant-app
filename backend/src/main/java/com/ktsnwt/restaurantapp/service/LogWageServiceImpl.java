package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.dto.reports.WageDateDTO;
import com.ktsnwt.restaurantapp.exception.InvalidReportTimeIntervalException;
import com.ktsnwt.restaurantapp.model.LogWage;
import com.ktsnwt.restaurantapp.model.User;
import com.ktsnwt.restaurantapp.repository.LogWageRepository;
import com.ktsnwt.restaurantapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LogWageServiceImpl implements LogWageService{

    @Autowired
    private LogWageRepository logWageRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<WageDateDTO> getEmployeeWageTimeInterval(String username, LocalDate startDate, LocalDate endDate) throws InvalidReportTimeIntervalException {

        if(startDate.isAfter(endDate)) throw  new InvalidReportTimeIntervalException("Start date after end date ! ! ! ! !");

        List<LogWage> logs = logWageRepository.findByOrderByTimestampAsc();

        logs = logs.stream().filter(logWage -> logWage.getEmployee().getUsername().toUpperCase().contains(username.toUpperCase())).collect(Collectors.toList());

        List<WageDateDTO> data = new ArrayList<>();

        while(!startDate.equals(endDate)){

            data.add(new WageDateDTO(getValueForDate(username,startDate,logs), startDate));
            startDate = startDate.plusDays(1);
        }
        return data;
    }

    @Override
    public List<WageDateDTO> getAverageRoleWageTimeInterval(String role, LocalDate startDate, LocalDate endDate) throws InvalidReportTimeIntervalException {

        if(startDate.isAfter(endDate)) throw  new InvalidReportTimeIntervalException("Start date after end date ! ! ! ! !");

        List<LogWage> logs = logWageRepository.findByOrderByTimestampAsc();

        logs = logs.stream().filter(logWage -> logWage.getEmployee().getRole().toUpperCase().contains(role.toUpperCase())).collect(Collectors.toList());

        List<WageDateDTO> data = new ArrayList<>();

        while(!startDate.equals(endDate)){

            data.add(new WageDateDTO(getValueForDateRole(role,startDate,logs), startDate));
            startDate = startDate.plusDays(1);
        }
        return data;

    }

    public Double getValueForDate(String username, LocalDate date, List<LogWage> logWages){
        for(LogWage log: logWages){
            if(!log.getTimestamp().toLocalDate().isBefore(ChronoLocalDate.from(date))){return log.getOldValue();}
        }

        User u = userRepository.findByUsername(username);
        if (u != null) return userRepository.findByUsername(username).getWage();
        return 0.0;
    }


    public Double getValueForDateRole(String username, LocalDate date, List<LogWage> logWages){
        Double value = (double) 0;
        List<User> employees = userRepository.findByRoleContainingIgnoreCaseAndDeletedIsFalse(username);
       Integer count = employees.size();
        for(LogWage log: logWages){
            if(!log.getTimestamp().toLocalDate().isBefore(ChronoLocalDate.from(date)))
            {
                if(employees.contains(log.getEmployee())){
                    value += log.getOldValue();
                    employees.remove(log.getEmployee());
                }
            }
        }
        for(User e: employees){
            if(e.getWage()!=null){
                value+=e.getWage();
            }
            else count--;
        }
        if(count <= 0) return 0.0;
        return value/count;
    }

}
