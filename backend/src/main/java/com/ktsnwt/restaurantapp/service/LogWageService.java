package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.dto.reports.WageDateDTO;
import com.ktsnwt.restaurantapp.exception.InvalidReportTimeIntervalException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;


public interface LogWageService {

    List<WageDateDTO> getEmployeeWageTimeInterval(String username, LocalDate startDate, LocalDate endDate) throws InvalidReportTimeIntervalException;

    List<WageDateDTO> getAverageRoleWageTimeInterval(String role, LocalDate startDate, LocalDate endDate) throws InvalidReportTimeIntervalException;

}
