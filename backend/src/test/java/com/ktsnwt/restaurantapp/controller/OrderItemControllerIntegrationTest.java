package com.ktsnwt.restaurantapp.controller;

import com.ktsnwt.restaurantapp.dto.NewOrderDTO;
import com.ktsnwt.restaurantapp.dto.NewOrderItemDTO;
import com.ktsnwt.restaurantapp.utils.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.integration.test.context.SpringIntegrationTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@SpringIntegrationTest
@WebAppConfiguration
public class OrderItemControllerIntegrationTest {
    private static final String URL_PREFIX = "/api/orderItems";

    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype());

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(springSecurity()).build();
    }

    @Test
    @WithMockUser(username = "waiter1", roles = {"WAITER"})
    @Transactional
    @Rollback
    public void testServeNonExistingOrderItem() throws Exception {
        mockMvc.perform(post(URL_PREFIX + "/serveOrderItem?id=100")
                .contentType(contentType)).andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "waiter1", roles = {"WAITER"})
    @Transactional
    @Rollback
    public void testServeOrderItem() throws Exception {
        mockMvc.perform(post(URL_PREFIX + "/serveOrderItem?id=5")
                .contentType(contentType)).andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "barman1", roles = {"BARMAN"})
    @Transactional
    @Rollback
    public void testAcceptOrderItem() throws Exception {
        mockMvc.perform(post(URL_PREFIX + "/acceptOrderItem?id=5")
                .contentType(contentType)).andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "chef1", roles = {"CHEF"})
    @Transactional
    @Rollback
    public void testGetOrderItemsForChef() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/getOrderItems?role=ROLE_CHEF"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(equalTo(3))));
    }

    @Test
    @WithMockUser(username = "barman1", roles = {"BARMAN"})
    @Transactional
    @Rollback
    public void testGetOrderItemsForBarman() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/getOrderItems?role=ROLE_BARMAN"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(equalTo(2))));
    }

}
