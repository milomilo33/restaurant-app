package com.ktsnwt.restaurantapp.controller;

import com.ktsnwt.restaurantapp.dto.NewOrderDTO;
import com.ktsnwt.restaurantapp.dto.NewOrderItemDTO;
import com.ktsnwt.restaurantapp.utils.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.integration.test.context.SpringIntegrationTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@SpringIntegrationTest
@WebAppConfiguration
public class OrderControllerIntegrationTest {
    private static final String URL_PREFIX = "/api/orders";

    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype());

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username = "waiter1", roles = {"WAITER"})
    @Transactional
    @Rollback
    public void testCreateOrderWithNonExistingMenuItemId() throws Exception {
        NewOrderDTO newOrderDTO = new NewOrderDTO();
        newOrderDTO.setNoteIn("Integration");
        newOrderDTO.setLayoutTableId(2L);
        List<NewOrderItemDTO> newOrderItemDTOList = new ArrayList<>();
        NewOrderItemDTO newOrderItemDTO = new NewOrderItemDTO();
        newOrderItemDTO.setMenuItemId(1000L);
        newOrderItemDTO.setNoteIn("Integration");
        newOrderItemDTO.setQuantity(3);
        newOrderItemDTOList.add(newOrderItemDTO);
        newOrderDTO.setOrderItemDTOS(newOrderItemDTOList);

        String json = TestUtils.json(newOrderDTO);
        mockMvc.perform(post(URL_PREFIX + "/createOrder")
                .contentType(contentType).content(json)).andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "waiter1", roles = {"WAITER"})
    @Transactional
    @Rollback
    public void testNewOrderNotCreated() throws Exception {
        NewOrderDTO newOrderDTO = new NewOrderDTO();
        newOrderDTO.setOrderItemDTOS(new ArrayList<>());

        String json = TestUtils.json(newOrderDTO);
        mockMvc.perform(post(URL_PREFIX + "/createOrder")
                .contentType(contentType).content(json)).andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "waiter1", roles = {"WAITER"})
    @Transactional
    @Rollback
    public void testCreateNewOrder() throws Exception {
        NewOrderDTO newOrderDTO = new NewOrderDTO();
        newOrderDTO.setNoteIn("Integration");
        newOrderDTO.setLayoutTableId(2L);
        List<NewOrderItemDTO> newOrderItemDTOList = new ArrayList<>();
        NewOrderItemDTO newOrderItemDTO = new NewOrderItemDTO();
        newOrderItemDTO.setMenuItemId(1L);
        newOrderItemDTO.setNoteIn("Integration");
        newOrderItemDTO.setQuantity(3);
        newOrderItemDTOList.add(newOrderItemDTO);
        newOrderDTO.setOrderItemDTOS(newOrderItemDTOList);

        String json = TestUtils.json(newOrderDTO);
        mockMvc.perform(post(URL_PREFIX + "/createOrder")
                .contentType(contentType).content(json))
                .andExpect(status().isOk());

    }

    @Test
    @WithMockUser(username = "waiter1", roles = {"WAITER"})
    public void testSearchOrders() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/search?criteria=Incomplete&table=2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(equalTo(1))));
    }

    @Test
    @WithMockUser(username = "waiter1", roles = {"WAITER"})
    public void testIsOrderChargeableWithNonExistingOrderId() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/isOrderChargeable?id=100"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "waiter1", roles = {"WAITER"})
    public void testIsOrderChargeable() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/isOrderChargeable?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));
    }

    @Test
    @WithMockUser(username = "waiter1", roles = {"WAITER"})
    public void testIsOrderNotChargeable() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/isOrderChargeable?id=2"))
                .andExpect(status().isOk())
                .andExpect(content().string("false"));
    }

    @Test
    @WithMockUser(username = "waiter1", roles = {"WAITER"})
    @Transactional
    @Rollback
    public void testChargeNonExistingOrder() throws Exception {
        mockMvc.perform(post(URL_PREFIX + "/chargeOrder?id=10"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "waiter1", roles = {"WAITER"})
    @Transactional
    @Rollback
    public void testChargeOrder() throws Exception {
        mockMvc.perform(post(URL_PREFIX + "/chargeOrder?id=1"))
                .andExpect(status().isOk());
    }
}
