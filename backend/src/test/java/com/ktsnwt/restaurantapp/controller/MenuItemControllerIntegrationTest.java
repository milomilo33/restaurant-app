package com.ktsnwt.restaurantapp.controller;

import com.ktsnwt.restaurantapp.model.MenuItem;
import com.ktsnwt.restaurantapp.utils.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.integration.test.context.SpringIntegrationTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@SpringIntegrationTest
@WebAppConfiguration
public class MenuItemControllerIntegrationTest {
    private static final String URL_PREFIX = "/api/menu-item";

    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype());

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetValidMenuItem() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetInvalidMenuItem() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/100"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testUpdateInvalidMenuItem() throws Exception {
        Long id = 100L;
        String name = "abc";
        String ingredients = "abc";
        Double price = 100.0;
        String allergens = "abc";
        Double waitingTime = 100.0;
        String category = "abc";
        Double ingredientCost = 100.0;
        boolean available = true;
        byte[] image = null;
        boolean suggestion = true;
        boolean food = true;
        boolean deleted = false;

        MenuItem mi = new MenuItem(id,name,ingredients,price,allergens,waitingTime,
                category,ingredientCost,available,image,suggestion,food,deleted);
        String json = TestUtils.json(mi);
        System.out.println(json);

        mockMvc.perform(post(URL_PREFIX + "/update")
                .flashAttr("menuItem", mi))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testUpdateMenuItemInvalidPrice() throws Exception {
        Long id = 1L;
        String name = "abc";
        String ingredients = "abc";
        Double price = -100.0;
        String allergens = "abc";
        Double waitingTime = 100.0;
        String category = "abc";
        Double ingredientCost = 100.0;
        boolean available = true;
        byte[] image = null;
        boolean suggestion = true;
        boolean food = true;
        boolean deleted = false;

        MenuItem mi = new MenuItem(id,name,ingredients,price,allergens,waitingTime,
                category,ingredientCost,available,image,suggestion,food,deleted);
        String json = TestUtils.json(mi);

        mockMvc.perform(post(URL_PREFIX + "/update")
                .flashAttr("menuItem", mi))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testUpdateMenuItemValid() throws Exception {
        Long id = 1L;
        String name = "abc";
        String ingredients = "abc";
        Double price = 100.0;
        String allergens = "abc";
        Double waitingTime = 100.0;
        String category = "abc";
        Double ingredientCost = 100.0;
        boolean available = true;
        byte[] image = null;
        boolean suggestion = true;
        boolean food = true;
        boolean deleted = false;

        MenuItem mi = new MenuItem(id,name,ingredients,price,allergens,waitingTime,
                category,ingredientCost,available,image,suggestion,food,deleted);
        String json = TestUtils.json(mi);

        mockMvc.perform(post(URL_PREFIX + "/update")
                .flashAttr("menuItem", mi))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testAddInvalidPriceMenuItemToMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/add-to-menu?menuId=2&menuItemId=4&price=-100"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testAddExistingMenuItemToMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/add-to-menu?menuId=1&menuItemId=1&price=100"))
                .andExpect(status().isConflict());
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testAddValidMenuItemToMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/add-to-menu?menuId=2&menuItemId=4&price=100"))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testRemoveInvalidMenuItemFromMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/remove-from-menu?menuId=2&menuItemId=4"))
                .andExpect(status().isConflict());
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testRemoveValidMenuItemFromMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/remove-from-menu?menuId=2&menuItemId=3"))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testUpdatePriceForMenuItemInMenuInvalidPrice() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/update-in-menu?menuId=2&menuItemId=3&price=-123"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testUpdatePriceForMenuItemInMenuNotInMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/update-in-menu?menuId=2&menuItemId=4&price=123"))
                .andExpect(status().isConflict());
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testUpdatePriceForMenuItemInMenuValid() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/update-in-menu?menuId=2&menuItemId=3&price=123"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testSearchSortFilterMenuItemsInvalidParamSortOption() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/search?pageIndex=0&pageSize=10&name=a&allergens=a&typeCriteria=Any&availableCriteria=Any&category=Any&sortAscOrDesc=asc&sortOption=INVALID"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testSearchSortFilterMenuItemsInvalidParamTypeCriteria() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/search?pageIndex=0&pageSize=10&name=a&allergens=a&typeCriteria=INVALID&availableCriteria=Any&category=Any&sortAscOrDesc=asc&sortOption=name"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testSearchSortFilterMenuItemsInvalidParamAvailableCriteria() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/search?pageIndex=0&pageSize=10&name=a&allergens=a&typeCriteria=Any&availableCriteria=INVALID&category=Any&sortAscOrDesc=asc&sortOption=name"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testSearchSortFilterMenuItemsValidParams() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/search?pageIndex=0&pageSize=10&name=belo&allergens=&typeCriteria=Any&availableCriteria=Any&category=Any&sortAscOrDesc=asc&sortOption=name"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageNumber").value(0))
                .andExpect(jsonPath("$.pageNumberOfElements").value(1))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.totalPages").value(1))
                .andExpect(jsonPath("$.menuItemsInPage[0].name").value("Belo vino"));
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testSearchSortFilterMenuItemsForMenuInvalidParamInMenuCriteria() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/searchForMenu?pageIndex=0&pageSize=10&name=&allergens=&typeCriteria=Any&availableCriteria=Any&category=Any&sortAscOrDesc=asc&sortOption=name&inMenuCriteria=INVALID&menuId=2"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testSearchSortFilterMenuItemsForMenuInvalidParamMenuId() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/searchForMenu?pageIndex=0&pageSize=10&name=&allergens=&typeCriteria=Any&availableCriteria=Any&category=Any&sortAscOrDesc=asc&sortOption=name&inMenuCriteria=INVALID&menuId=10"))
                .andExpect(status().isBadRequest());
    }
}
