package com.ktsnwt.restaurantapp.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.integration.test.context.SpringIntegrationTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@SpringIntegrationTest
@WebAppConfiguration
public class ReportsControllerTests {

    private static final String URL_PREFIX = "/api/reports";

    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype());

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetEmployeeWageTimeInterval() throws Exception {
            mockMvc.perform(get(URL_PREFIX +"/employee?" + "username=manager1&start_date=2021-01-29&end_date=2022-01-29"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(equalTo(365))));

    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetRoleAverageWageTimeInterval() throws Exception {
        mockMvc.perform(get(URL_PREFIX +"/role?" + "role=manager1&start_date=2021-01-29&end_date=2022-01-29"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(equalTo(365))));

    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetProfitTimeInterval() throws Exception {
        mockMvc.perform(get(URL_PREFIX +"/menu-item?" + "name=Espreso&start_date=2021-01-29&end_date=2022-01-29"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(equalTo(365))));

    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetPrepareCostTimeInterval() throws Exception {
        mockMvc.perform(get(URL_PREFIX +"/menu-item-prepare?" + "name=Espreso&start_date=2021-01-29&end_date=2022-01-29"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(equalTo(365))));

    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetPriceSoldTimeInterval() throws Exception {
        mockMvc.perform(get(URL_PREFIX +"/menu-item-price?" + "name=Espreso&start_date=2021-01-29&end_date=2022-01-29"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(equalTo(365))));

    }


}
