package com.ktsnwt.restaurantapp.controller;

import com.ktsnwt.restaurantapp.dto.manager.UserForManagerTableDTO;
import com.ktsnwt.restaurantapp.utils.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.integration.test.context.SpringIntegrationTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@SpringIntegrationTest
@WebAppConfiguration
public class UserControllerTests {
    private static final String URL_PREFIX = "/api";

    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype());

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    @Rollback
    @Transactional
    public void testDeleteStaff() throws Exception {
        this.mockMvc.perform(delete(URL_PREFIX + "/staff/barman1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    @Rollback
    @Transactional
    public void testPutStaff() throws Exception {
        UserForManagerTableDTO u = new UserForManagerTableDTO();
        u.setUsername("barman1");
        u.setEmailAddress("new@new.com");
        u.setType("BARMAN");
        u.setPhoneNumber("3930429234");
        u.setFirstName("New First Name");
        u.setLastName("New Last Name");
        u.setWage(613379.0);

        String json = TestUtils.json(u);

        this.mockMvc.perform(put(URL_PREFIX + "/staff")
                .contentType(contentType).content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("New First Name"))
                .andExpect(jsonPath("$.phoneNumber").value("3930429234"))
                .andExpect(jsonPath("$.lastName").value("New Last Name"))
                .andExpect(jsonPath("$.emailAddress").value("new@new.com"));
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetStaffByUsername() throws Exception {
        this.mockMvc.perform(get(URL_PREFIX + "/staff?username=barman1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetStaffCount() throws Exception {
        this.mockMvc.perform(get(URL_PREFIX + "/staff/count"))
                .andExpect(status().isOk())
                .andExpect(content().string("7"));
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})

    public void testGetAllStaffSearch() throws Exception {
        this.mockMvc.perform(get(URL_PREFIX + "/staffs?pageIndex=0&pageSize=10&direction=asc&active=username&search=&type=ALL&wage=0&greater=GREATER"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(equalTo(7))));
    }


    @Test
    @WithMockUser(username = "admin1", roles = {"ADMIN"})
    @Rollback
    @Transactional
    public void testDeleteEmployee() throws Exception {
        this.mockMvc.perform(delete(URL_PREFIX + "/employee/manager1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "admin1", roles = {"ADMIN"})
    @Rollback
    @Transactional
    public void testPutEmployee() throws Exception {
        UserForManagerTableDTO u = new UserForManagerTableDTO();
        u.setUsername("manager1");
        u.setEmailAddress("new@new.com");
        u.setType("MANAGER");
        u.setPhoneNumber("3930429234");
        u.setFirstName("New First Name");
        u.setLastName("New Last Name");
        u.setWage(613379.0);

        String json = TestUtils.json(u);

        this.mockMvc.perform(put(URL_PREFIX + "/employee")
                        .contentType(contentType).content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("New First Name"))
                .andExpect(jsonPath("$.phoneNumber").value("3930429234"))
                .andExpect(jsonPath("$.lastName").value("New Last Name"))
                .andExpect(jsonPath("$.emailAddress").value("new@new.com"));
    }

    @Test
    @WithMockUser(username = "admin1", roles = {"ADMIN"})
    public void testGetEmployeeByUsername() throws Exception {
        this.mockMvc.perform(get(URL_PREFIX + "/employee?username=barman1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "admin1", roles = {"ADMIN"})
    public void testGetEmployeeCount() throws Exception {
        this.mockMvc.perform(get(URL_PREFIX + "/employee/count"))
                .andExpect(status().isOk())
                .andExpect(content().string("9"));
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetAllEmployeeSearch() throws Exception {
        this.mockMvc.perform(get(URL_PREFIX + "/employees?pageIndex=0&pageSize=10&direction=asc&active=username&search=&type=ALL&wage=0&greater=GREATER"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(equalTo(9))));
    }



}
