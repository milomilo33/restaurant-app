package com.ktsnwt.restaurantapp.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.integration.test.context.SpringIntegrationTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@SpringIntegrationTest
@WebAppConfiguration
public class MenuControllerIntegrationTest {
    private static final String URL_PREFIX = "/api/menu";

    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype());

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetValidMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetInvalidMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/5"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testDeleteActiveMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/delete/1"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testDeleteInactiveMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/delete/2"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testGetActiveMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/active"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    @Transactional
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testUpdateMenu() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/update?id=1&name=newNow"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testSearchSortFilterMenusInvalidParamActive() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/search?name=xd&activeCriteria=INVALID&sortAscOrDesc=asc&pageIndex=1&pageSize=5"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "manager1", roles = {"MANAGER"})
    public void testSearchSortFilterMenusValidParams() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/search?name=zimski&activeCriteria=any&sortAscOrDesc=asc&pageIndex=0&pageSize=5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageNumber").value(0))
                .andExpect(jsonPath("$.pageNumberOfElements").value(1))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.totalPages").value(1))
                .andExpect(jsonPath("$.menusInPage[0].name").value("Zimski meni"));
    }
}
