package com.ktsnwt.restaurantapp.service;


import com.ktsnwt.restaurantapp.exception.InvalidReportTimeIntervalException;
import com.ktsnwt.restaurantapp.model.User;
import com.ktsnwt.restaurantapp.repository.LogRepository;
import com.ktsnwt.restaurantapp.repository.LogWageRepository;
import com.ktsnwt.restaurantapp.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class LogWageServiceUnitTest {

    @Autowired
    private LogWageServiceImpl logWageService;

    @Mock
    private UserRepository userRepository;


    private final LocalDate startDate = LocalDate.now();
    private final LocalDate endDate = LocalDate.now().minusDays(1);

    Integer firstFactor = 235;

    Integer secondFactor = 140;

    private final LocalDate startDateActual = LocalDate.now().minusDays(firstFactor);
    private final LocalDate endDateActual = LocalDate.now().minusDays(secondFactor);


    @Test
    public void shouldThrowExceptionWhenBadDateTimeIntervals(){
        Assertions.assertThrows(
                InvalidReportTimeIntervalException.class,
                () -> logWageService.getAverageRoleWageTimeInterval("someName", startDate, endDate)
        );

    }

    @Test
    public void shouldThrowExceptionWhenBadDateTimeIntervalsPrepare(){

        Assertions.assertThrows(
                InvalidReportTimeIntervalException.class,
                () -> logWageService.getEmployeeWageTimeInterval("someName", startDate, endDate)
        );

    }


    @Test
    public void shouldGenerateNNumberOfValuesWhereNIsDateDifference() throws InvalidReportTimeIntervalException {
        Assertions.assertEquals(
                logWageService.getEmployeeWageTimeInterval("someName", startDateActual, endDateActual).size(),
                firstFactor - secondFactor
        );

        Assertions.assertEquals(
                logWageService.getAverageRoleWageTimeInterval("someName", startDateActual, endDateActual).size(),
                firstFactor - secondFactor
        );

    }

}
