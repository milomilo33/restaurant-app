package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.constants.UserConstants;
import com.ktsnwt.restaurantapp.exception.OrderItemNotFoundException;
import com.ktsnwt.restaurantapp.exception.UserNotFoundException;
import com.ktsnwt.restaurantapp.model.Order;
import com.ktsnwt.restaurantapp.model.OrderItem;
import com.ktsnwt.restaurantapp.model.OrderStatus;
import com.ktsnwt.restaurantapp.repository.OrderItemRepository;
import com.ktsnwt.restaurantapp.service.OrderItemService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class OrderItemServiceIntegrationTest {

    @Autowired
    OrderItemService orderItemService;

    @Autowired
    OrderItemRepository orderItemRepository;

    @BeforeEach
    void setUp(){

    }

    @Test
    void testGetOrderItemsForWaiter(){
        List<OrderItem> orderItems = orderItemService.getOrderItems("ROLE_WAITER");

        for(OrderItem orderItem: orderItems){
            Assertions.assertEquals(orderItem.getOrderStatus(), OrderStatus.READY);
        }
        Assertions.assertEquals(3, orderItems.size());
    }

    @Test
    @Transactional
    @Rollback
    void testChargeOrderItem() throws OrderItemNotFoundException {
        Order order = orderItemService.chargeOrderItem(5L);
        OrderItem orderItem = orderItemRepository.getById(5L);
        Assertions.assertEquals(0.0, order.getTotalPrice() - orderItem.getMenuItem().getPrice()*orderItem.getQuantity());
    }

    @Test
    @Transactional
    @Rollback
    void testChargeNonExistingOrderItem() {
        Assertions.assertThrows(
                OrderItemNotFoundException.class,() ->orderItemService.chargeOrderItem(100L));
    }

    @Test
    @Transactional
    @Rollback
    void testCancelOrderItem() throws OrderItemNotFoundException {
        Order order = orderItemService.cancelOrderItem(5L);
        OrderItem orderItem = orderItemRepository.getById(5L);
        Assertions.assertEquals(OrderStatus.CANCELLED, orderItem.getOrderStatus());
    }

    @Test
    @Transactional
    @Rollback
    void testCancelNonExistingOrderItem() {
        Assertions.assertThrows(
                OrderItemNotFoundException.class,() ->orderItemService.cancelOrderItem(100L));
    }
}
