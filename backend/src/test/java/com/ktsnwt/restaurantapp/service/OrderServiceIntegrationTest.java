package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.dto.NewOrderDTO;
import com.ktsnwt.restaurantapp.dto.NewOrderItemDTO;
import com.ktsnwt.restaurantapp.exception.MenuItemNotFoundException;
import com.ktsnwt.restaurantapp.exception.OrderItemNotFoundException;
import com.ktsnwt.restaurantapp.exception.OrderNotFoundException;
import com.ktsnwt.restaurantapp.model.Order;
import com.ktsnwt.restaurantapp.model.OrderItem;
import com.ktsnwt.restaurantapp.model.OrderStatus;
import com.ktsnwt.restaurantapp.service.OrderService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class OrderServiceIntegrationTest {

    @Autowired
    OrderService orderService;

    NewOrderDTO newOrderDTO;

    @BeforeEach
    void setUp(){
        newOrderDTO = new NewOrderDTO();
        newOrderDTO.setOrderItemDTOS(new ArrayList<>());
        for(int i=0;i<5;i++) {
            NewOrderItemDTO orderItem = new NewOrderItemDTO();
            orderItem.setMenuItemId(1L + i);
            orderItem.setQuantity(i + 5);
            orderItem.setNoteIn("Integration service note " + i);
            newOrderDTO.getOrderItemDTOS().add(orderItem);
        }
        newOrderDTO.setLayoutTableId(4L);
    }

    @Test
    @Transactional
    @Rollback
    void testAddOrder() throws MenuItemNotFoundException {
        Order order = orderService.addOrder(newOrderDTO);

        for(OrderItem orderItem: order.getOrderItems())
            Assertions.assertEquals(OrderStatus.PLACED, orderItem.getOrderStatus());
        Assertions.assertEquals(0.0, order.getTotalPrice());
        Assertions.assertFalse(order.isCompleted());
    }

    @Test
    @Transactional
    @Rollback
    void testAddOrderWithNonExistingMenuItem() {
        newOrderDTO.getOrderItemDTOS().add(new NewOrderItemDTO(100L, 10, "Integration test note", null));

        Assertions.assertThrows(
                MenuItemNotFoundException.class,() ->orderService.addOrder(newOrderDTO));
    }

    @Test
    @Transactional
    @Rollback
    void testSearchOrders() throws MenuItemNotFoundException {
        Order order = orderService.addOrder(newOrderDTO);
        List<Order> orders = orderService.searchOrders("Incomplete", "any");

        Assertions.assertEquals(3, orders.size());

        orders = orderService.searchOrders("Completed", "any");

        Assertions.assertEquals(1, orders.size());

        orders = orderService.searchOrders("any", "4");

        Assertions.assertEquals(1, orders.size());
    }

    @Test
    void testIsOrderChargeable() throws OrderNotFoundException {
        Assertions.assertFalse(orderService.isOrderChargeable(2L));
    }

    @Test
    void testIsOrderChargeableNonExistingOrder() {
        Assertions.assertThrows(
                OrderNotFoundException.class,() ->orderService.isOrderChargeable(100L));
    }

    @Test
    @Transactional
    @Rollback
    void testChargeOrderNotComplete() throws OrderNotFoundException {
        Order order = orderService.chargeOrder(2L);

        Assertions.assertFalse(order.isCompleted());
    }

    @Test
    @Transactional
    @Rollback
    void testChargeNonExistingOrder() throws OrderNotFoundException {
        Assertions.assertThrows(
                OrderNotFoundException.class,() ->orderService.chargeOrder(100L));
    }
}
