package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.constants.LogConstants;
import com.ktsnwt.restaurantapp.constants.UserConstants;
import com.ktsnwt.restaurantapp.dto.reports.WageDateDTO;
import com.ktsnwt.restaurantapp.exception.InvalidReportTimeIntervalException;
import com.ktsnwt.restaurantapp.service.LogWageServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class LogWageServiceIntegrationTest {

    @Autowired
    private LogWageServiceImpl logWageService;

    @Test
    public void testGetEmployeeWageTimeInterval() throws InvalidReportTimeIntervalException {
        List<WageDateDTO> data = logWageService.getEmployeeWageTimeInterval(UserConstants.USERNAME, LocalDate.now().minusDays(200), LocalDate.now().minusDays(100));

        Assertions.assertEquals(data.size(), 100);
    }

    @Test
    public void testGetAverageRoleWageTimeInterval() throws InvalidReportTimeIntervalException {
        List<WageDateDTO> data = logWageService.getAverageRoleWageTimeInterval("WAITER", LocalDate.now().minusDays(200), LocalDate.now().minusDays(100));

        Assertions.assertEquals(data.size(), 100);
    }
}
