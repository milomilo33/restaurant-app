package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.exception.MenuItemNotFoundException;
import com.ktsnwt.restaurantapp.exception.OrderItemNotFoundException;
import com.ktsnwt.restaurantapp.model.MenuItem;
import com.ktsnwt.restaurantapp.model.Order;
import com.ktsnwt.restaurantapp.model.OrderItem;
import com.ktsnwt.restaurantapp.model.OrderStatus;
import com.ktsnwt.restaurantapp.repository.OrderItemRepository;
import com.ktsnwt.restaurantapp.repository.OrderRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class OrderItemServiceUnitTest {
    @Autowired
    OrderItemService orderItemServiceMock;

    @MockBean
    OrderItemRepository orderItemRepositoryMock;

    @MockBean
    OrderRepository orderRepositoryMock;

    private OrderItem mockOrderItem;
    private OrderItem cancelledOrderItem;

    @BeforeEach
    void setUp(){
        mockOrderItem = new OrderItem();
        mockOrderItem.setId(1L);
        mockOrderItem.setOrderStatus(OrderStatus.PLACED);

        cancelledOrderItem = new OrderItem();
        cancelledOrderItem.setId(1L);
        cancelledOrderItem.setOrderStatus(OrderStatus.CANCELLED);
    }

    @Test
    void shouldReturnOrderContainingCancelledOrderItemWhenCancelingOrderItem() throws OrderItemNotFoundException {
        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        order.setOrderItems(new HashSet<>());
        order.getOrderItems().add(cancelledOrderItem);
        orders.add(order);

        Mockito.when(orderItemRepositoryMock.findById(1L)).thenReturn(Optional.of(mockOrderItem));
        Mockito.when(orderItemRepositoryMock.save(cancelledOrderItem)).thenReturn(cancelledOrderItem);
        Mockito.when(orderRepositoryMock.findAll()).thenReturn(orders);

        assertTrue(orderItemServiceMock.cancelOrderItem(1L).getOrderItems().stream().allMatch(orderItem -> orderItem.getOrderStatus().equals(OrderStatus.CANCELLED)));
    }

    @Test
    void shouldThrowOrderItemNotFoundExceptionWhenCancelingNonExistingOrder(){
        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        order.setOrderItems(new HashSet<>());
        order.getOrderItems().add(cancelledOrderItem);
        orders.add(order);

        Mockito.when(orderItemRepositoryMock.findById(1L)).thenReturn(Optional.of(mockOrderItem));
        Mockito.when(orderItemRepositoryMock.save(cancelledOrderItem)).thenReturn(cancelledOrderItem);
        Mockito.when(orderRepositoryMock.findAll()).thenReturn(orders);

        Assertions.assertThrows(
                OrderItemNotFoundException.class,
                () -> orderItemServiceMock.cancelOrderItem(2L)
        );
    }

    @Test
    void shouldReturnPlacedOrderItemsContainingFoodMenuItemsWhenUserRoleIsChef(){
        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        order.setOrderItems(new HashSet<>());
        MenuItem menuItem = new MenuItem();
        menuItem.setId(1L);
        menuItem.setFood(true);
        mockOrderItem.setMenuItem(menuItem);
        order.getOrderItems().add(mockOrderItem);
        orders.add(order);
        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(mockOrderItem);

        Mockito.when(orderItemRepositoryMock.findAll()).thenReturn(orderItems);

        assertTrue(orderItemServiceMock.getOrderItems("ROLE_CHEF").stream().allMatch(orderItem -> orderItem.getOrderStatus().equals(OrderStatus.PLACED)  && orderItem.getMenuItem().isFood()));

    }

    @Test
    void shouldReturnEmptyListWhenUserRoleIsChef(){
        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        order.setOrderItems(new HashSet<>());
        MenuItem menuItem = new MenuItem();
        menuItem.setId(1L);
        menuItem.setFood(false);
        mockOrderItem.setMenuItem(menuItem);
        order.getOrderItems().add(mockOrderItem);
        orders.add(order);
        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(mockOrderItem);

        Mockito.when(orderItemRepositoryMock.findAll()).thenReturn(orderItems);

        assertTrue(orderItemServiceMock.getOrderItems("ROLE_CHEF").stream().allMatch(orderItem -> orderItem.getOrderStatus().equals(OrderStatus.PLACED)  && orderItem.getMenuItem().isFood()));

    }
}
