package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.dto.RestaurantLayoutDTO;
import com.ktsnwt.restaurantapp.model.Layout;
import com.ktsnwt.restaurantapp.model.LayoutTable;
import com.ktsnwt.restaurantapp.repository.LayoutRepository;
import com.ktsnwt.restaurantapp.repository.LayoutTableRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class LayoutServiceUnitTest {
    @Autowired
    private LayoutService layoutServiceMock;
    @MockBean
    private LayoutRepository layoutRepositoryMock;
    @MockBean
    private LayoutTableRepository layoutTableRepositoryMock;

    private static List<Layout> mockLayouts;

    @BeforeAll
    public static void setUp(){
        Layout layout = new Layout();
        layout.setImage("mock_image.png");
        layout.setDeleted(false);
        layout.setActive(true);
        layout.setId(1L);
        layout.setName("Mock");
        Set<LayoutTable> layoutTableSet = new HashSet<>();
        for(int i = 0; i < 10; i++) {
            LayoutTable layoutTable = new LayoutTable();
            layoutTable.setId(i + 1L);
            layoutTable.setCapacity(3);
            layoutTable.setX(i);
            layoutTable.setY(i + 100);
            layoutTable.setDeleted(i >= 5);//5
            layoutTable.setSmoking(true);
            layoutTable.setDisplayNumber(i);
            layoutTable.setOccupied(true);
            layoutTableSet.add(layoutTable);
        }
        layout.setLayoutTables(layoutTableSet);
        mockLayouts = new ArrayList<>();
        mockLayouts.add(layout);
    }

    @Test
    public void shouldReturnLayoutWithFiveLayoutTables() {
        Mockito.when(layoutRepositoryMock.findAll()).thenReturn(mockLayouts);

        RestaurantLayoutDTO restaurantLayoutDTO = layoutServiceMock.getLayout();
        assertEquals(5, restaurantLayoutDTO.getTablePositions().size());
        assertEquals("mock_image.png", restaurantLayoutDTO.getImgUrl());
    }

    @Test
    public void shouldReturnNonDeletedLayoutTableDisplayNames() {
        Mockito.when(layoutRepositoryMock.findAll()).thenReturn(mockLayouts);

        List<Integer> tableDisplays = layoutServiceMock.getLayoutTableDisplays();
        AtomicInteger i = new AtomicInteger();
        i.set(0);
        Set<LayoutTable> layoutTables = mockLayouts.get(0).getLayoutTables().stream().filter(layoutTable -> !layoutTable.isDeleted()).collect(Collectors.toSet());
        assertTrue(layoutTables.stream().allMatch(layoutTable -> layoutTable.getDisplayNumber().equals(tableDisplays.get(i.getAndIncrement()))));
    }

}
