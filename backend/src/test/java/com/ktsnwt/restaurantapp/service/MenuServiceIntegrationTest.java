package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.exception.InvalidMenuForDeletionException;
import com.ktsnwt.restaurantapp.exception.InvalidSearchSortFilterParamsException;
import com.ktsnwt.restaurantapp.exception.ResourceNotFoundException;
import com.ktsnwt.restaurantapp.service.MenuServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class MenuServiceIntegrationTest {

    @Autowired
    private MenuServiceImpl menuService;

    @BeforeAll
    static void setUp() {

    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenInvalidId() {
        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuService.findOne(10L)
        );
    }

    @Test
    @Rollback
    public void shouldReturnMenuWhenValidId() throws ResourceNotFoundException {
        Assertions.assertNotNull(
                menuService.findOne(1L)
        );
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenMenuDeleted() {
        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuService.findOne(3L)
        );
    }

    @Test
    @Rollback
    public void shouldReturnMenusWhenMenusAvailable() throws ResourceNotFoundException {
        Assertions.assertEquals(2, menuService.findAll().size());
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenMenuToDeleteIsDeleted() {
        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuService.deleteMenu(3L)
        );
    }

    @Test
    @Rollback
    public void shouldThrowInvalidMenuForDeletionExceptionWhenMenuToDeleteIsActive() {
        Assertions.assertThrows(
                InvalidMenuForDeletionException.class,
                () -> menuService.deleteMenu(1L)
        );
    }

    @Test
    @Transactional
    @Rollback
    public void shouldDeleteMenuWhenValidId() throws ResourceNotFoundException, InvalidMenuForDeletionException {
        menuService.deleteMenu(2L);

        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuService.findOne(2L)
        );
    }

    @Test
    @Transactional
    @Rollback
    public void shouldActivateMenuAndDeactivateTheOtherWhenValidId() throws ResourceNotFoundException {
        menuService.activateMenu(2L);

        Assertions.assertFalse(menuService.findOne(1L).isActive());
        Assertions.assertTrue(menuService.findOne(2L).isActive());
    }

    @Test
    @Transactional
    @Rollback
    public void shouldChangeNameWhenValidId() throws ResourceNotFoundException {
        menuService.updateMenu(1L, "New name yasss");

        Assertions.assertEquals("New name yasss", menuService.findOne(1L).getName());
    }

    @Test
    @Rollback
    public void shouldThrowInvalidSearchSortFilterParamsExceptionWhenInvalidActiveParam() {
        InvalidSearchSortFilterParamsException exc = Assertions.assertThrows(
                InvalidSearchSortFilterParamsException.class,
                () -> menuService.searchFilterSortMenus("zimski", "invalid", "asc", 0, 10)
        );

        Assertions.assertEquals("Invalid param 'active'.", exc.getMessage());
    }

    @Test
    @Rollback
    public void shouldReturnMenusWhenValidParams() throws InvalidSearchSortFilterParamsException {
        var result = menuService.searchFilterSortMenus("zimski", "any", "asc", 0, 10);

        Assertions.assertEquals(0, result.getPageNumber());
        Assertions.assertEquals(1, result.getPageNumberOfElements());
        Assertions.assertEquals(1, result.getTotalElements());
        Assertions.assertEquals(1, result.getTotalPages());
        Assertions.assertTrue(result.getMenusInPage().get(0).getName().toLowerCase().contains("zimski"));
    }

}
