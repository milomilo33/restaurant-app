package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.dto.MenuDTO;
import com.ktsnwt.restaurantapp.dto.MenuSearchSortFilterDTO;
import com.ktsnwt.restaurantapp.exception.InvalidMenuForDeletionException;
import com.ktsnwt.restaurantapp.exception.InvalidSearchSortFilterParamsException;
import com.ktsnwt.restaurantapp.exception.ResourceNotFoundException;
import com.ktsnwt.restaurantapp.model.Menu;
import com.ktsnwt.restaurantapp.model.MenuItem;
import com.ktsnwt.restaurantapp.repository.MenuRepository;
import com.ktsnwt.restaurantapp.service.MenuServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.annotation.Rollback;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;

@SpringBootTest
public class MenuServiceUnitTest {
    @Mock
    private MenuRepository menuRepositoryMock;

    @InjectMocks
    private MenuServiceImpl menuServiceMock;

    private Menu menuMock;
    private Menu menuMock2;
    private Menu menuMock3;
    private List<Menu> menusMock;

    @BeforeEach
    public void Setup() {
        MenuItem menuItem1 = new MenuItem();
        menuItem1.setDeleted(false);
        menuItem1.setAllergens("Kajmak");
        menuItem1.setAvailable(true);
        menuItem1.setCategory("Srpska jela");
        menuItem1.setFood(true);
        menuItem1.setIngredientCost(250.0);
        menuItem1.setName("Karađorđeva šnicla");
        menuItem1.setPrice(800.0);
        menuItem1.setSuggestion(true);
        menuItem1.setWaitingTime(25.0);

        MenuItem menuItem2 = new MenuItem();
        menuItem2.setDeleted(false);
        menuItem2.setAllergens("");
        menuItem2.setAvailable(true);
        menuItem2.setCategory("Alkoholna pića");
        menuItem2.setFood(false);
        menuItem2.setIngredientCost(150.0);
        menuItem2.setName("Belo vino");
        menuItem2.setPrice(400.0);
        menuItem2.setSuggestion(true);
        menuItem2.setWaitingTime(1.0);

        Map<MenuItem, Double> menuItems = new HashMap<>();
        menuItems.put(menuItem1, 800.0);
        menuItems.put(menuItem2, 400.0);

        menuMock = new Menu("Zimski", true, false);
        menuMock.setId(1L);
        menuMock.setMenuItems(menuItems);

        menuMock2 = new Menu("Letnji", false, true);
        menuMock2.setId(5L);

        menuMock3 = new Menu("Za brisanje", false, false);
        menuMock3.setId(6L);

        menusMock = new ArrayList<>();
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenInvalidId() {
        Mockito.doReturn(Optional.of(menuMock)).when(menuRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuMock2)).when(menuRepositoryMock).findById(5L);

        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuServiceMock.findOne(2L)
        );
    }

    @Test
    @Rollback
    public void shouldReturnMenuWhenValidId() throws ResourceNotFoundException {
        Mockito.doReturn(Optional.of(menuMock)).when(menuRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuMock2)).when(menuRepositoryMock).findById(5L);

        Assertions.assertNotNull(
                menuServiceMock.findOne(1L)
        );
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenMenuDeleted() {
        Mockito.doReturn(Optional.of(menuMock)).when(menuRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuMock2)).when(menuRepositoryMock).findById(5L);

        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuServiceMock.findOne(5L)
        );
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenNoMenus() {
        Mockito.doReturn(menusMock).when(menuRepositoryMock).findByDeletedFalse();

        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuServiceMock.findAll()
        );
    }

    @Test
    @Rollback
    public void shouldReturnMenusWhenMenusAvailable() throws ResourceNotFoundException {
        menusMock.add(menuMock);
        Mockito.doReturn(menusMock).when(menuRepositoryMock).findByDeletedFalse();

        Assertions.assertEquals(1, menuServiceMock.findAll().size());
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenMenuToDeleteIsDeleted() {
        Mockito.doReturn(Optional.of(menuMock)).when(menuRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuMock2)).when(menuRepositoryMock).findById(5L);

        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuServiceMock.deleteMenu(5L)
        );
    }

    @Test
    @Rollback
    public void shouldThrowInvalidMenuForDeletionExceptionWhenMenuToDeleteIsActive() {
        Mockito.doReturn(Optional.of(menuMock)).when(menuRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuMock2)).when(menuRepositoryMock).findById(5L);

        Assertions.assertThrows(
                InvalidMenuForDeletionException.class,
                () -> menuServiceMock.deleteMenu(1L)
        );
    }

    @Test
    @Rollback
    public void shouldDeleteMenuWhenValidId() throws ResourceNotFoundException, InvalidMenuForDeletionException {
        Mockito.doReturn(Optional.of(menuMock)).when(menuRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuMock2)).when(menuRepositoryMock).findById(5L);
        Mockito.doReturn(Optional.of(menuMock3)).when(menuRepositoryMock).findById(6L);

        menuServiceMock.deleteMenu(6L);

        Assertions.assertTrue(menuMock3.isDeleted());
    }

    @Test
    @Rollback
    public void shouldActivateMenuAndDeactivateTheOtherWhenValidId() throws ResourceNotFoundException {
        Mockito.doReturn(Optional.of(menuMock)).when(menuRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuMock2)).when(menuRepositoryMock).findById(5L);
        Mockito.doReturn(Optional.of(menuMock3)).when(menuRepositoryMock).findById(6L);

        menusMock.add(menuMock);
        menusMock.add(menuMock3);
        Mockito.doReturn(menusMock).when(menuRepositoryMock).findByDeletedFalse();

        menuServiceMock.activateMenu(6L);

        Assertions.assertFalse(menusMock.get(0).isActive());
        Assertions.assertTrue(menusMock.get(1).isActive());
    }

    @Test
    @Rollback
    public void shouldChangeNameWhenValidId() throws ResourceNotFoundException {
        Mockito.doReturn(Optional.of(menuMock)).when(menuRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuMock2)).when(menuRepositoryMock).findById(5L);
        Mockito.doReturn(Optional.of(menuMock3)).when(menuRepositoryMock).findById(6L);

        menuServiceMock.updateMenu(1L, "New name yasss");

        Assertions.assertEquals("New name yasss", menuMock.getName());
    }

    @Test
    @Rollback
    public void shouldThrowInvalidSearchSortFilterParamsExceptionWhenInvalidActiveParam() {
        InvalidSearchSortFilterParamsException exc = Assertions.assertThrows(
                InvalidSearchSortFilterParamsException.class,
                () -> menuServiceMock.searchFilterSortMenus("Zimski", "invalid", "asc", 0, 10)
        );

        Assertions.assertEquals("Invalid param 'active'.", exc.getMessage());
    }

//    @Test
//    @Rollback
//    public void shouldReturnValidSearchResultDTOWhenValidParams() throws InvalidSearchSortFilterParamsException {
//        menusMock.add(menuMock);
//
//        Pageable pageable =  PageRequest.of(0, 10, Sort.by("name").descending());
//        Page<Menu> pagedResponse = new PageImpl(menusMock, pageable, 1);
//        System.out.println(pagedResponse.getNumber());
////        var pageableAny = any(Pageable.class);
////        Mockito.doReturn(pagedResponse).when(menuRepositoryMock).searchSortFilter("Zimski", null, pageableAny);
//        Mockito.when(menuRepositoryMock.searchSortFilter(isA(String.class), isA(Boolean.class), isA(Pageable.class))).thenReturn(pagedResponse);
//
//        MenuSearchSortFilterDTO retVal = menuServiceMock.searchFilterSortMenus("Zimski", "Any", "asc", 0, 10);
//
//        Assertions.assertEquals(retVal.getPageNumber(), 0);
//        Assertions.assertEquals(retVal.getPageNumberOfElements(), 1);
//        Assertions.assertEquals(retVal.getTotalElements(), 1);
//        Assertions.assertEquals(retVal.getTotalPages(), 1);
//        Assertions.assertEquals(retVal.getMenusInPage().get(0).getId(), 1L);
//    }
}
