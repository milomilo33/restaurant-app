package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.constants.LogConstants;
import com.ktsnwt.restaurantapp.constants.UserConstants;
import com.ktsnwt.restaurantapp.dto.reports.WageDateDTO;
import com.ktsnwt.restaurantapp.exception.InvalidReportTimeIntervalException;
import com.ktsnwt.restaurantapp.service.LogServiceImpl;
import com.ktsnwt.restaurantapp.service.LogWageServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.util.List;

@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class LogServiceIntegrationTest {

    @Autowired
    private LogServiceImpl logService;

    @Test
    public void testGetSoldInTimePeriodForItem() throws InvalidReportTimeIntervalException {
        List<WageDateDTO> data = logService.getSoldInTimePeriodForItem(LogConstants.DISN_NAME, LocalDate.now().minusDays(200), LocalDate.now().minusDays(100));

        Assertions.assertEquals(data.size(), 100);
    }

    @Test
    public void testGetSoldInTimePeriodForItemPrice() throws InvalidReportTimeIntervalException {
        List<WageDateDTO> data = logService.getSoldInTimePeriodForItemPrice(LogConstants.DISN_NAME, LocalDate.now().minusDays(200), LocalDate.now().minusDays(100));

        Assertions.assertEquals(data.size(), 100);
    }

    @Test
    public void testGetSoldInTimePeriodForItemPrepare() throws InvalidReportTimeIntervalException {
        List<WageDateDTO> data = logService.getSoldInTimePeriodForItemPrepare(LogConstants.DISN_NAME, LocalDate.now().minusDays(200), LocalDate.now().minusDays(100));

        Assertions.assertEquals(data.size(), 100);
    }
}
