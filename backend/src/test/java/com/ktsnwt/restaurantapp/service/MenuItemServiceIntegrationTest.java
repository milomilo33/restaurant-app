package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.exception.*;
import com.ktsnwt.restaurantapp.model.MenuItem;
import com.ktsnwt.restaurantapp.service.MenuItemServiceImpl;
import com.ktsnwt.restaurantapp.service.MenuServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class MenuItemServiceIntegrationTest {

    @Autowired
    private MenuItemServiceImpl menuItemService;

    @Autowired
    private MenuServiceImpl menuService;

    @BeforeAll
    static void setUp() {

    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenInvalidId() {
        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuItemService.findOne(100L)
        );
    }

    @Test
    @Rollback
    public void shouldReturnMenuItemWhenValidId() throws ResourceNotFoundException {
        Assertions.assertEquals(1L, menuItemService.findOne(1L).getId());
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenMenuItemDeleted() {
        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuItemService.findOne(7L)
        );
    }

    @Test
    @Rollback
    public void shouldReturnMenusWhenMenusAvailable() throws ResourceNotFoundException {
        Assertions.assertEquals(6, menuItemService.findAll().size());
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenMenuItemToDeleteIsDeleted() {
        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuItemService.deleteMenuItem(7L)
        );
    }

    @Test
    @Transactional
    @Rollback
    public void shouldDeleteMenuItemWhenValidId() throws ResourceNotFoundException {
        menuItemService.deleteMenuItem(1L);

        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuItemService.findOne(1L)
        );
    }

    @Test
    @Rollback
    public void shouldThrowInvalidMenuItemPriceExceptionWhenInvalidPrice() {
        Assertions.assertThrows(
                InvalidMenuItemPriceException.class,
                () -> menuItemService.addMenuItemToMenu(2L, 4L, -100.0)
        );
    }

    @Test
    @Rollback
    public void shouldThrowMenuItemAlreadyInMenuExceptionWhenMenuItemAlreadyInMenu() {
        Assertions.assertThrows(
                MenuItemAlreadyInMenuException.class,
                () -> menuItemService.addMenuItemToMenu(1L, 1L, 100.0)
        );
    }

    @Test
    @Transactional
    @Rollback
    public void shouldAddMenuItemToMenuWhenAllValid() throws InvalidMenuItemPriceException, MenuItemAlreadyInMenuException, ResourceNotFoundException {
        menuItemService.addMenuItemToMenu(2L, 5L, 100.0);

        var menuItemsPrice = menuService.findOne(2L).getMenuItems();
        MenuItem mi = null;
        Double price = null;
        for (var entry : menuItemsPrice.entrySet()) {
            if (entry.getKey().getId().equals(5L)) {
                mi = entry.getKey();
                price = entry.getValue();
                break;
            }
        }

        Assertions.assertNotNull(mi);
        Assertions.assertEquals(100.0, price);
    }

    @Test
    @Rollback
    public void shouldThrowMenuItemNotFoundInMenuExceptionWhenMenuItemNotInMenuForDeletion() {
        Assertions.assertThrows(
                MenuItemNotFoundInMenuException.class,
                () -> menuItemService.removeMenuItemFromMenu(2L, 4L)
        );
    }

    @Test
    @Transactional
    @Rollback
    public void shouldRemoveMenuItemFromMenuWhenAllValid() throws ResourceNotFoundException, MenuItemNotFoundInMenuException {
        menuItemService.removeMenuItemFromMenu(1L, 1L);

        var menuItemsPrice = menuService.findOne(1L).getMenuItems();
        for (var entry : menuItemsPrice.entrySet()) {
            Assertions.assertNotEquals(1L, (long) entry.getKey().getId());
        }
    }

    @Test
    @Transactional
    @Rollback
    public void shouldUpdatePriceForMenuItemInMenuWhenAllValid() throws MenuItemNotFoundInMenuException, InvalidMenuItemPriceException, ResourceNotFoundException {
        menuItemService.updatePriceForMenuItemInMenu(1L, 1L, 69.69);

        var menuItemsPrice = menuService.findOne(1L).getMenuItems();
        MenuItem mi = null;
        Double price = null;
        for (var entry : menuItemsPrice.entrySet()) {
            if (entry.getKey().getId().equals(1L)) {
                mi = entry.getKey();
                price = entry.getValue();
                break;
            }
        }

        Assertions.assertEquals(69.69, price);
    }

    @Test
    @Rollback
    public void shouldThrowInvalidSearchSortFilterParamsExceptionWhenInvalidSortOptionParam() {
        InvalidSearchSortFilterParamsException exc = Assertions.assertThrows(
                InvalidSearchSortFilterParamsException.class,
                () -> menuItemService.searchFilterSortMenuItems("a", "allergens", "any",
                        "any", "any", "invalid", "asc", 0, 10)
        );

        Assertions.assertEquals("Invalid param 'sortOption'.", exc.getMessage());
    }

    @Test
    @Rollback
    public void shouldThrowInvalidSearchSortFilterParamsExceptionWhenInvalidTypeCriteriaParam() {
        InvalidSearchSortFilterParamsException exc = Assertions.assertThrows(
                InvalidSearchSortFilterParamsException.class,
                () -> menuItemService.searchFilterSortMenuItems("a", "allergens", "invalid",
                        "any", "any", "name", "asc", 0, 10)
        );

        Assertions.assertEquals("Invalid param 'typeCriteria'.", exc.getMessage());
    }

    @Test
    @Rollback
    public void shouldThrowInvalidSearchSortFilterParamsExceptionWhenInvalidAvailableCriteriaParam() {
        InvalidSearchSortFilterParamsException exc = Assertions.assertThrows(
                InvalidSearchSortFilterParamsException.class,
                () -> menuItemService.searchFilterSortMenuItems("a", "allergens", "any",
                        "invalid", "any", "name", "asc", 0, 10)
        );

        Assertions.assertEquals("Invalid param 'availableCriteria'.", exc.getMessage());
    }

    @Test
    @Rollback
    public void shouldReturnMenuItemsWhenValidParams() throws InvalidSearchSortFilterParamsException {
        var result = menuService.searchFilterSortMenus("a", "any", "asc", 0, 10);

        Assertions.assertEquals(0, result.getPageNumber());
        Assertions.assertTrue(result.getPageNumberOfElements() <= 10);
        for (var menuInPage : result.getMenusInPage()) {
            Assertions.assertTrue(menuInPage.getName().toLowerCase().contains("a"));
        }
    }

    @Test
    @Rollback
    public void shouldThrowInvalidSearchSortFilterParamsExceptionWhenInvalidInMenuCriteriaParam() {
        InvalidSearchSortFilterParamsException exc = Assertions.assertThrows(
                InvalidSearchSortFilterParamsException.class,
                () -> menuItemService.searchFilterSortMenuItemsForMenu("a", "allergens", "any",
                        "any", "any", "name", "asc", 0, 10, "invalid", 1L)
        );

        Assertions.assertEquals("Invalid param 'inMenuCriteria'.", exc.getMessage());
    }

}
