package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.exception.InvalidReportTimeIntervalException;
import com.ktsnwt.restaurantapp.repository.LogRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class LogServiceUnitTest {

    @Autowired
    private LogServiceImpl logService;


    private final LocalDate startDate = LocalDate.now();
    private final LocalDate endDate = LocalDate.now().minusDays(1);

    Integer firstFactor = 235;

    Integer secondFactor = 140;

    private final LocalDate startDateActual = LocalDate.now().minusDays(firstFactor);
    private final LocalDate endDateActual = LocalDate.now().minusDays(secondFactor);

    @Test
    public void shouldThrowExceptionWhenBadDateTimeIntervals(){
        Assertions.assertThrows(
                InvalidReportTimeIntervalException.class,
                () -> logService.getSoldInTimePeriodForItem("someName", startDate, endDate)
        );

    }

    @Test
    public void shouldThrowExceptionWhenBadDateTimeIntervalsPrepare(){

        Assertions.assertThrows(
                InvalidReportTimeIntervalException.class,
                () -> logService.getSoldInTimePeriodForItemPrepare("someName", startDate, endDate)
        );

    }

    @Test
    public void shouldThrowExceptionWhenBadDateTimeIntervalsPrice(){

        Assertions.assertThrows(
                InvalidReportTimeIntervalException.class,
                () -> logService.getSoldInTimePeriodForItemPrice("someName", startDate, endDate)
        );
    }


    @Test
    public void shouldGenerateNNumberOfValuesWhereNIsDateDifference() throws InvalidReportTimeIntervalException {

        Assertions.assertEquals(
             logService.getSoldInTimePeriodForItemPrice("someName", startDateActual, endDateActual).size(),
               firstFactor - secondFactor
        );

        Assertions.assertEquals(
                logService.getSoldInTimePeriodForItem("someName", startDateActual, endDateActual).size(),
                firstFactor - secondFactor
        );

        Assertions.assertEquals(
                logService.getSoldInTimePeriodForItemPrepare("someName", startDateActual, endDateActual).size(),
                firstFactor - secondFactor
        );
    }


}
