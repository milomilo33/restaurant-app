package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.exception.*;
import com.ktsnwt.restaurantapp.model.Menu;
import com.ktsnwt.restaurantapp.model.MenuItem;
import com.ktsnwt.restaurantapp.repository.MenuItemRepository;
import com.ktsnwt.restaurantapp.repository.MenuRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;

import java.util.*;

@SpringBootTest
public class MenuItemServiceUnitTest {
    @Mock
    private MenuItemRepository menuItemRepositoryMock;

    @Mock
    private MenuRepository menuRepositoryMock;

    @Mock
    private MenuService menuServiceMock;

    @InjectMocks
    private MenuItemServiceImpl menuItemServiceMock;

    private Menu menuMock;
    private Menu menuMock2;
    private Menu menuMock3;
    private List<Menu> menusMock;

    private MenuItem menuItem1;
    private MenuItem menuItem2;
    private MenuItem menuItemDeleted;

    private List<MenuItem> menuItemsMock;

    @BeforeEach
    public void Setup() {
        menuItem1 = new MenuItem();
        menuItem1.setId(1L);
        menuItem1.setDeleted(false);
        menuItem1.setAllergens("Kajmak");
        menuItem1.setAvailable(true);
        menuItem1.setCategory("Srpska jela");
        menuItem1.setFood(true);
        menuItem1.setIngredientCost(250.0);
        menuItem1.setName("Karađorđeva šnicla");
        menuItem1.setPrice(800.0);
        menuItem1.setSuggestion(true);
        menuItem1.setWaitingTime(25.0);

        menuItem2 = new MenuItem();
        menuItem2.setId(2L);
        menuItem2.setDeleted(false);
        menuItem2.setAllergens("");
        menuItem2.setAvailable(true);
        menuItem2.setCategory("Alkoholna pića");
        menuItem2.setFood(false);
        menuItem2.setIngredientCost(150.0);
        menuItem2.setName("Belo vino");
        menuItem2.setPrice(400.0);
        menuItem2.setSuggestion(true);
        menuItem2.setWaitingTime(1.0);

        Map<MenuItem, Double> menuItems = new HashMap<>();
        menuItems.put(menuItem1, 800.0);
        menuItems.put(menuItem2, 400.0);

        menuItemDeleted = new MenuItem();
        menuItemDeleted.setId(3L);
        menuItemDeleted.setDeleted(true);
        menuItemDeleted.setAllergens("Alergen");
        menuItemDeleted.setAvailable(true);
        menuItemDeleted.setCategory("Sopska jela");
        menuItemDeleted.setFood(true);
        menuItemDeleted.setIngredientCost(150.0);
        menuItemDeleted.setName("Sopska salata");
        menuItemDeleted.setPrice(500.0);
        menuItemDeleted.setSuggestion(true);
        menuItemDeleted.setWaitingTime(10.0);

        menuMock = new Menu("Zimski", true, false);
        menuMock.setId(1L);
        menuMock.setMenuItems(menuItems);

        menuMock2 = new Menu("Letnji", false, true);
        menuMock2.setId(5L);

        menuMock3 = new Menu("Za brisanje", false, false);
        menuMock3.setId(6L);

        menusMock = new ArrayList<>();
        menuItemsMock = new ArrayList<>();
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenInvalidId() {
        Mockito.doReturn(Optional.of(menuItem1)).when(menuItemRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuItem2)).when(menuItemRepositoryMock).findById(2L);
        Mockito.doReturn(Optional.of(menuItemDeleted)).when(menuItemRepositoryMock).findById(3L);

        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuItemServiceMock.findOne(10L)
        );
    }

    @Test
    @Rollback
    public void shouldReturnMenuItemWhenValidId() throws ResourceNotFoundException {
        Mockito.doReturn(Optional.of(menuItem1)).when(menuItemRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuItem2)).when(menuItemRepositoryMock).findById(2L);
        Mockito.doReturn(Optional.of(menuItemDeleted)).when(menuItemRepositoryMock).findById(3L);

        Assertions.assertEquals(1L, menuItemServiceMock.findOne(1L).getId());
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenMenuItemDeleted() {
        Mockito.doReturn(Optional.of(menuItem1)).when(menuItemRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuItem2)).when(menuItemRepositoryMock).findById(2L);
        Mockito.doReturn(Optional.of(menuItemDeleted)).when(menuItemRepositoryMock).findById(3L);

        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuItemServiceMock.findOne(3L)
        );
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenNoMenus() {
        Mockito.doReturn(menuItemsMock).when(menuItemRepositoryMock).findByDeletedFalse();

        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuItemServiceMock.findAll()
        );
    }

    @Test
    @Rollback
    public void shouldReturnMenusWhenMenusAvailable() throws ResourceNotFoundException {
        menuItemsMock.add(menuItem1);
        menuItemsMock.add(menuItem2);
        Mockito.doReturn(menuItemsMock).when(menuItemRepositoryMock).findByDeletedFalse();

        Assertions.assertEquals(2, menuItemServiceMock.findAll().size());
    }

    @Test
    @Rollback
    public void shouldThrowResourceNotFoundExceptionWhenMenuItemToDeleteIsDeleted() {
        Mockito.doReturn(Optional.of(menuItem1)).when(menuItemRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuItem2)).when(menuItemRepositoryMock).findById(2L);
        Mockito.doReturn(Optional.of(menuItemDeleted)).when(menuItemRepositoryMock).findById(3L);

        Assertions.assertThrows(
                ResourceNotFoundException.class,
                () -> menuItemServiceMock.deleteMenuItem(3L)
        );
    }

    @Test
    @Rollback
    public void shouldDeleteMenuItemWhenValidId() throws ResourceNotFoundException {
        Mockito.doReturn(Optional.of(menuItem1)).when(menuItemRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuItem2)).when(menuItemRepositoryMock).findById(2L);
        Mockito.doReturn(Optional.of(menuItemDeleted)).when(menuItemRepositoryMock).findById(3L);

        menuItemServiceMock.deleteMenuItem(2L);

        Assertions.assertTrue(menuItem2.isDeleted());
    }

    @Test
    @Rollback
    public void shouldThrowInvalidMenuItemPriceExceptionWhenInvalidPrice() throws ResourceNotFoundException {
        Mockito.doReturn(Optional.of(menuItem1)).when(menuItemRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuItem2)).when(menuItemRepositoryMock).findById(2L);
        Mockito.doReturn(Optional.of(menuItemDeleted)).when(menuItemRepositoryMock).findById(3L);

        Mockito.doReturn(menuMock).when(menuServiceMock).findOne(1L);

        Assertions.assertThrows(
                InvalidMenuItemPriceException.class,
                () -> menuItemServiceMock.addMenuItemToMenu(1L, 1L, -100.0)
        );
    }

    @Test
    @Rollback
    public void shouldThrowMenuItemAlreadyInMenuExceptionWhenMenuItemAlreadyInMenu() throws ResourceNotFoundException {
        Mockito.doReturn(Optional.of(menuItem1)).when(menuItemRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuItem2)).when(menuItemRepositoryMock).findById(2L);
        Mockito.doReturn(Optional.of(menuItemDeleted)).when(menuItemRepositoryMock).findById(3L);

        Mockito.doReturn(menuMock).when(menuServiceMock).findOne(1L);

        Assertions.assertThrows(
                MenuItemAlreadyInMenuException.class,
                () -> menuItemServiceMock.addMenuItemToMenu(1L, 1L, 100.0)
        );
    }

    @Test
    @Rollback
    public void shouldAddMenuItemToMenuWhenAllValid() throws ResourceNotFoundException, InvalidMenuItemPriceException, MenuItemAlreadyInMenuException {
        menuItemDeleted.setDeleted(false);

        Mockito.doReturn(Optional.of(menuItem1)).when(menuItemRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuItem2)).when(menuItemRepositoryMock).findById(2L);
        Mockito.doReturn(Optional.of(menuItemDeleted)).when(menuItemRepositoryMock).findById(3L);

        Mockito.doReturn(menuMock).when(menuServiceMock).findOne(1L);

        menuItemServiceMock.addMenuItemToMenu(1L, 3L, 100.0);

        Assertions.assertEquals(100.0, menuMock.getMenuItems().get(menuItemDeleted));
    }

    @Test
    @Rollback
    public void shouldThrowMenuItemNotFoundInMenuExceptionWhenMenuItemNotInMenuForDeletion() throws ResourceNotFoundException {
        menuItemDeleted.setDeleted(false);

        Mockito.doReturn(Optional.of(menuItem1)).when(menuItemRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuItem2)).when(menuItemRepositoryMock).findById(2L);
        Mockito.doReturn(Optional.of(menuItemDeleted)).when(menuItemRepositoryMock).findById(3L);

        Mockito.doReturn(menuMock).when(menuServiceMock).findOne(1L);

        Assertions.assertThrows(
                MenuItemNotFoundInMenuException.class,
                () -> menuItemServiceMock.removeMenuItemFromMenu(1L, 3L)
        );
    }

    @Test
    @Rollback
    public void shouldRemoveMenuItemFromMenuWhenAllValid() throws ResourceNotFoundException, MenuItemNotFoundInMenuException {
        Mockito.doReturn(Optional.of(menuItem1)).when(menuItemRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuItem2)).when(menuItemRepositoryMock).findById(2L);
        Mockito.doReturn(Optional.of(menuItemDeleted)).when(menuItemRepositoryMock).findById(3L);

        Mockito.doReturn(menuMock).when(menuServiceMock).findOne(1L);

        menuItemServiceMock.removeMenuItemFromMenu(1L, 1L);

        Assertions.assertFalse(menuMock.getMenuItems().containsKey(menuItem1));
    }

    @Test
    @Rollback
    public void shouldUpdatePriceForMenuItemInMenuWhenAllValid() throws ResourceNotFoundException, MenuItemNotFoundInMenuException, InvalidMenuItemPriceException {
        Mockito.doReturn(Optional.of(menuItem1)).when(menuItemRepositoryMock).findById(1L);
        Mockito.doReturn(Optional.of(menuItem2)).when(menuItemRepositoryMock).findById(2L);
        Mockito.doReturn(Optional.of(menuItemDeleted)).when(menuItemRepositoryMock).findById(3L);

        Mockito.doReturn(menuMock).when(menuServiceMock).findOne(1L);

        menuItemServiceMock.updatePriceForMenuItemInMenu(1L, 1L, 69.69);

        Assertions.assertEquals(69.69, menuMock.getMenuItems().get(menuItem1));
    }

    @Test
    @Rollback
    public void shouldThrowInvalidSearchSortFilterParamsExceptionWhenInvalidSortOptionParam() {
        InvalidSearchSortFilterParamsException exc = Assertions.assertThrows(
                InvalidSearchSortFilterParamsException.class,
                () -> menuItemServiceMock.searchFilterSortMenuItems("a", "allergens", "any",
                        "any", "any", "invalid", "asc", 0, 10)
        );

        Assertions.assertEquals("Invalid param 'sortOption'.", exc.getMessage());
    }

    @Test
    @Rollback
    public void shouldThrowInvalidSearchSortFilterParamsExceptionWhenInvalidTypeCriteriaParam() {
        InvalidSearchSortFilterParamsException exc = Assertions.assertThrows(
                InvalidSearchSortFilterParamsException.class,
                () -> menuItemServiceMock.searchFilterSortMenuItems("a", "allergens", "invalid",
                        "any", "any", "name", "asc", 0, 10)
        );

        Assertions.assertEquals("Invalid param 'typeCriteria'.", exc.getMessage());
    }

    @Test
    @Rollback
    public void shouldThrowInvalidSearchSortFilterParamsExceptionWhenInvalidAvailableCriteriaParam() {
        InvalidSearchSortFilterParamsException exc = Assertions.assertThrows(
                InvalidSearchSortFilterParamsException.class,
                () -> menuItemServiceMock.searchFilterSortMenuItems("a", "allergens", "any",
                        "invalid", "any", "name", "asc", 0, 10)
        );

        Assertions.assertEquals("Invalid param 'availableCriteria'.", exc.getMessage());
    }

    @Test
    @Rollback
    public void shouldThrowInvalidSearchSortFilterParamsExceptionWhenInvalidInMenuCriteriaParam() {
        InvalidSearchSortFilterParamsException exc = Assertions.assertThrows(
                InvalidSearchSortFilterParamsException.class,
                () -> menuItemServiceMock.searchFilterSortMenuItemsForMenu("a", "allergens", "any",
                        "any", "any", "name", "asc", 0, 10, "invalid", 1L)
        );

        Assertions.assertEquals("Invalid param 'inMenuCriteria'.", exc.getMessage());
    }
}
