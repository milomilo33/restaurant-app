package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.dto.NewOrderDTO;
import com.ktsnwt.restaurantapp.dto.NewOrderItemDTO;
import com.ktsnwt.restaurantapp.exception.MenuItemNotFoundException;
import com.ktsnwt.restaurantapp.exception.OrderNotFoundException;
import com.ktsnwt.restaurantapp.exception.UserNotFoundException;
import com.ktsnwt.restaurantapp.model.*;
import com.ktsnwt.restaurantapp.repository.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class OrderServiceUnitTest {
    @Autowired
    OrderService orderServiceMock;

    @MockBean
    OrderRepository orderRepositoryMock;

    @MockBean
    OrderItemRepository orderItemRepositoryMock;

    @MockBean
    LayoutTableRepository layoutTableRepositoryMock;

    @MockBean
    MenuItemRepository menuItemRepositoryMock;

    @MockBean
    LogRepository logRepositoryMock;

    private static Order mockOrder;
    private static NewOrderDTO newOrderDTO;
    private static OrderItem mockOrderItem;
    private static MenuItem mockMenuItem;
    private static Log mockLog;

    @BeforeEach
    public void setup(){
        mockOrder = new Order();
        mockOrder.setCompleted(false);
        mockOrder.setTotalPrice(1000.0);
        mockOrder.setDeleted(false);
        mockOrder.setId(1L);
        mockOrder.setOrderItems(new HashSet<>());
        mockOrder.setLayoutTable(new LayoutTable());
        Set<OrderItem> orderItems = new HashSet<>();
        mockOrderItem = new OrderItem();
        mockOrderItem.setId(1L);
        mockOrderItem.setOrderStatus(OrderStatus.PLACED);
        MenuItem menuItem = new MenuItem();
        menuItem.setId(1L);
        menuItem.setDeleted(false);
        menuItem.setIngredientCost(100.0);
        menuItem.setPrice(100.0);
        menuItem.setName("Mock");
        mockOrderItem.setMenuItem(menuItem);
        mockOrderItem.setQuantity(3);
        mockLog = new Log();
        mockLog.setMenuItemName("Mock");
        mockLog.setQuantity(3);
        mockLog.setPrice(100.0);
        mockLog.setIngredientCost(menuItem.getIngredientCost());
        mockLog.setTimestamp(LocalDateTime.now());
        mockLog.setId(1L);

        orderItems.add(mockOrderItem);
        mockOrder.setOrderItems(orderItems);
        LayoutTable layoutTable = new LayoutTable();
        layoutTable.setId(1L);
        layoutTable.setDisplayNumber(1);
        mockOrder.setLayoutTable(layoutTable);

        newOrderDTO = new NewOrderDTO();
        NewOrderItemDTO orderItem = new NewOrderItemDTO();
        orderItem.setMenuItemId(1L);
        orderItem.setOrderItemId("1L");
        newOrderDTO.setOrderItemDTOS(new ArrayList<>());
        newOrderDTO.getOrderItemDTOS().add(orderItem);
    }

    @Test
    @Rollback
    public void shouldThrowMenuItemNotFoundExceptionWhenAddingOrder(){
        Mockito.when(layoutTableRepositoryMock.findById(1L)).thenReturn(Optional.of(new LayoutTable()));
        Mockito.when(menuItemRepositoryMock.findById(2L)).thenReturn(null);
        Mockito.when(orderItemRepositoryMock.save(new OrderItem())).thenReturn(new OrderItem());
        Mockito.when(orderRepositoryMock.save(mockOrder)).thenReturn(mockOrder);

        Assertions.assertThrows(
                MenuItemNotFoundException.class,
                () -> orderServiceMock.addOrder(newOrderDTO)
        );
    }

    @Test
    @Rollback
    public void shouldReturnOrderContainingOrderItemWithOrderStatusPLACED() throws MenuItemNotFoundException {
        Mockito.when(layoutTableRepositoryMock.findById(1L)).thenReturn(Optional.of(new LayoutTable()));
        Mockito.when(menuItemRepositoryMock.findById(1L)).thenReturn(Optional.of(new MenuItem()));
        Mockito.when(orderItemRepositoryMock.save(mockOrderItem)).thenReturn(mockOrderItem);
        Mockito.when(orderRepositoryMock.save(mockOrder)).thenReturn(mockOrder);

        Order order = orderServiceMock.addOrder(newOrderDTO);
        assertTrue(order.getOrderItems().stream().anyMatch(orderItem -> orderItem.getOrderStatus().equals(OrderStatus.PLACED)));
    }

    @Test
    @Rollback
    public void shouldReturnEmptyListWhenSearchingOrdersWithCriteriaChargeable(){
        List<Order> allOrders = new ArrayList<>();
        allOrders.add(mockOrder);
        Mockito.when(orderRepositoryMock.findAll()).thenReturn(allOrders);

        assertEquals(new ArrayList<>(), orderServiceMock.searchOrders("Chargeable", "1"));
    }

    @Test
    @Rollback
    public void shouldReturnIncompleteOrders(){
        List<Order> allOrders = new ArrayList<>();
        allOrders.add(mockOrder);
        Mockito.when(orderRepositoryMock.findAll()).thenReturn(allOrders);

        assertEquals(allOrders, orderServiceMock.searchOrders("Incomplete", "1"));
    }

    @Test
    @Rollback
    public void shouldReturnOrderChargeableFalse() throws OrderNotFoundException {
        Mockito.when(orderRepositoryMock.findOneById(1L)).thenReturn(mockOrder);

        assertFalse(orderServiceMock.isOrderChargeable(1L));
    }

    @Test
    @Rollback
    public void shouldReturnCompletedOrderWhenChargingOrder() throws OrderNotFoundException {
        for(OrderItem orderItem :mockOrder.getOrderItems())
            orderItem.setOrderStatus(OrderStatus.CHARGED);
        Mockito.when(orderRepositoryMock.findOneById(1L)).thenReturn(mockOrder);
        Mockito.when(orderRepositoryMock.save(mockOrder)).thenReturn(mockOrder);
        Mockito.when(logRepositoryMock.save(mockLog)).thenReturn(mockLog);

        assertTrue(orderServiceMock.chargeOrder(1L).isCompleted());
    }
}
