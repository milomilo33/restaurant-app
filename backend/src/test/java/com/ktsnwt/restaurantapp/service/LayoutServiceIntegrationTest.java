package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.dto.RestaurantLayoutDTO;
import com.ktsnwt.restaurantapp.service.LayoutService;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class LayoutServiceIntegrationTest {

    @Autowired
    LayoutService layoutService;

    @BeforeEach
    void setUp(){

    }

    @Test
    void testGetAllLayoutTableDisplayNames(){
        List<Integer> displayNames = layoutService.getLayoutTableDisplays();

        displayNames = displayNames.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
        Assertions.assertEquals(5, displayNames.size());
        for(int displayName = 1; displayName < 6; displayName++){
            Assertions.assertEquals(displayName, displayNames.get(displayName - 1));
        }
    }

    @Test
    void testGetLayout(){
        RestaurantLayoutDTO layout = layoutService.getLayout();

        Assertions.assertEquals(5, layout.getTablePositions().size());
        Assertions.assertEquals("example.png", layout.getImgUrl());
    }
}
