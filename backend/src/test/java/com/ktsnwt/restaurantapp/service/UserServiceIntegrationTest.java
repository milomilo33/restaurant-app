package com.ktsnwt.restaurantapp.service;

import com.ktsnwt.restaurantapp.constants.UserConstants;
import com.ktsnwt.restaurantapp.dto.NewUserDTO;
import com.ktsnwt.restaurantapp.dto.manager.UserForManagerTableDTO;
import com.ktsnwt.restaurantapp.exception.InvalidUserSearchCriteriaException;
import com.ktsnwt.restaurantapp.exception.UserNotFoundException;
import com.ktsnwt.restaurantapp.model.User;
import com.ktsnwt.restaurantapp.service.UserService;
import com.ktsnwt.restaurantapp.service.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class UserServiceIntegrationTest {
    @Autowired
    private UserServiceImpl userService;


    @BeforeAll
    static void setUp() {

    }

    @Test
    public void testGetAllUsers(){
        Assertions.assertEquals(userService.getAllUsers().size(), UserConstants.USER_COUNT);
    }

    @Test
    public void testGetOneUserById() throws UserNotFoundException {
        Assertions.assertEquals(userService.getOneUser(UserConstants.USER_ID).getId(), UserConstants.USER_ID );
    }


    @Test
    public void testGetAllEmployeesForAdminUsernameAscGreater() throws InvalidUserSearchCriteriaException {
        List<UserForManagerTableDTO> users = userService.getAllEmployees(
                false,
                0,
                10,
                "asc",
                "username",
                "",
                "ALL",
                1000.0,
                "greater"
        );

        for(int i = 0; i < users.size(); i++){
            Assertions.assertTrue(users.get(i).getWage() >= 1000.0);
            Assertions.assertFalse(users.get(i).getType().toUpperCase().contains("ADMIN"));
            if (i + 1 < users.size()){
                Assertions.assertTrue(users.get(i).getUsername().compareTo(users.get(i+1).getUsername()) <= 0);
            }
        }
        Assertions.assertTrue(users.size() <= 10);
    }


    @Test
    public void testGetAllEmployeesForManagerNameDescLesser() throws InvalidUserSearchCriteriaException {
        List<UserForManagerTableDTO> users = userService.getAllEmployees(
                true,
                0,
                10,
                "desc",
                "firstName",
                "",
                "ALL",
                9000.0,
                "lesser"
        );

        for(int i = 0; i < users.size(); i++){
            Assertions.assertTrue(users.get(i).getWage() <= 9000.0);
            Assertions.assertFalse(users.get(i).getType().toUpperCase().contains("ADMIN"));
            Assertions.assertFalse(users.get(i).getType().toUpperCase().contains("MANAGER"));
            if (i + 1 < users.size()){
                Assertions.assertTrue(users.get(i).getFirstName().compareTo(users.get(i+1).getFirstName()) >= 0);
            }
        }
        Assertions.assertTrue(users.size() <= 10);
    }

    @Test
    public void testGetAllEmployees(){
        List<UserForManagerTableDTO> users =  userService.getAllEmployees();
        for(UserForManagerTableDTO u  : users){
            Assertions.assertFalse(u.getType().toUpperCase().contains("ADMIN"));
        }
    }

    @Test
    public void testGetEmployeeByUsernameManager() throws UserNotFoundException {
        Assertions.assertTrue(userService.getEmployeeByUsername(false, UserConstants.USERNAME).getUsername().equalsIgnoreCase(UserConstants.USERNAME));
    }

    @Test
    public void testGetEmployeeByUsernameNonManager() {
        Assertions.assertThrows(
                 UserNotFoundException.class,() ->userService.getEmployeeByUsername(true, UserConstants.USERNAME));
    }

    @Test
    @Rollback
    @Transactional
    public void testUpdateEmployee() throws UserNotFoundException {
        UserForManagerTableDTO u = new UserForManagerTableDTO();
        u.setUsername(UserConstants.USERNAME);
        u.setEmailAddress("new@new.com");
        u.setType("MANAGER");
        u.setPhoneNumber("3930429234");
        u.setFirstName("New First Name");
        u.setLastName("New Last Name");
        u.setWage(613379.0);

        User user = userService.updateEmployee(false, u);

        Assertions.assertEquals(user.getMail(), u.getEmailAddress());
        Assertions.assertEquals(user.getWage(), u.getWage());
        Assertions.assertEquals(user.getName(), u.getFirstName());
        Assertions.assertEquals(user.getSurname(), u.getLastName());
        Assertions.assertEquals(user.getPhoneNumber(), u.getPhoneNumber());
    }

    @Test
    public void testGetEmployeeCount() {
        Assertions.assertEquals(userService.getEmployeeCount(false), 9);
        Assertions.assertEquals(userService.getEmployeeCount(true), 7);
    }

    @Test
    @Transactional
    @Rollback
    public void testNewManager() throws UserNotFoundException {
        NewUserDTO newUserDTO = new NewUserDTO();
        newUserDTO.setType("MANAGER");
        newUserDTO.setUsername("NekI_username");
        newUserDTO.setEmailAddress("neka_adresa@neka_adresa.na");
        newUserDTO.setPassword(newUserDTO.getUsername());
        newUserDTO.setFirstName("Neko prednje ime");
        newUserDTO.setLastName("Neko zadnje ime");
        newUserDTO.setWage(213123.2);
        newUserDTO.setAddress("Neka adresa");

        User u = userService.newManager(newUserDTO);

        Assertions.assertEquals(u.getUsername(), newUserDTO.getUsername());
        Assertions.assertEquals(u.getSurname(), newUserDTO.getLastName());
        Assertions.assertEquals(u.getName(), newUserDTO.getFirstName());
        Assertions.assertEquals(u.getPhoneNumber(), newUserDTO.getPhoneNumber());
        Assertions.assertEquals(u.getMail(), newUserDTO.getEmailAddress());
        Assertions.assertEquals(u.getAddress(), newUserDTO.getAddress());

    }

    @Test     @Transactional    @Rollback
    public void testNewEmployee() throws UserNotFoundException {
        NewUserDTO newUserDTO = new NewUserDTO();
        newUserDTO.setType("WAITER");
        newUserDTO.setUsername("NekI_username");
        newUserDTO.setEmailAddress("neka_adresa@neka_adresa.na");
        newUserDTO.setPassword(newUserDTO.getUsername());
        newUserDTO.setFirstName("Neko prednje ime");
        newUserDTO.setLastName("Neko zadnje ime");
        newUserDTO.setWage(213123.2);
        newUserDTO.setAddress("Neka adresa");

        User u = userService.newEmployee(newUserDTO);

        Assertions.assertEquals(u.getUsername(), newUserDTO.getUsername());
        Assertions.assertEquals(u.getSurname(), newUserDTO.getLastName());
        Assertions.assertEquals(u.getName(), newUserDTO.getFirstName());
        Assertions.assertEquals(u.getPhoneNumber(), newUserDTO.getPhoneNumber());
        Assertions.assertEquals(u.getMail(), newUserDTO.getEmailAddress());
        Assertions.assertEquals(u.getAddress(), newUserDTO.getAddress());

    }

    @Test     @Transactional @Rollback
    public void testDeleteEmployee() throws UserNotFoundException {
        String s = userService.deleteEmployee(false, UserConstants.USERNAME);

        Assertions.assertEquals(s, "Employee deleted");
        Assertions.assertThrows(UserNotFoundException.class ,() ->userService.getEmployeeByUsername(false, UserConstants.USERNAME));
    }


}
