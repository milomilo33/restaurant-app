package com.ktsnwt.restaurantapp.constants;

import java.time.LocalDateTime;

public class LogConstants {

    public static final String DISN_NAME = "Prebranac";
    public static final LocalDateTime START_DATE = LocalDateTime.now().minusDays(200);
    public static final LocalDateTime END_DATE = LocalDateTime.now().minusDays(140);
}
