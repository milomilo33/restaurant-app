package com.ktsnwt.restaurantapp.e2e.pages;

import com.ktsnwt.restaurantapp.utils.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditMenuPage {
    WebDriver webDriver;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-create-menu/div/mat-card/form/button")
    private WebElement updateMenuButton;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-create-menu/div/mat-card/form/mat-card-content/mat-form-field/div/div[1]/div/input")
    private WebElement textInput;

    public EditMenuPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void updateMenuButtonClick() {
        Utilities.clickableWait(webDriver, this.updateMenuButton, 10).click();
    }

    public WebElement getTextInput() {
        return Utilities.visibilityWait(webDriver, this.textInput, 10);
    }

    public void setTextInput(String value) {
        WebElement el = getTextInput();
        el.clear();
        el.sendKeys(value);
    }
}
