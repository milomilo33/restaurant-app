package com.ktsnwt.restaurantapp.e2e.pages;

import com.ktsnwt.restaurantapp.utils.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OrderItemsPage {

    private WebDriver webDriver;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-order-items-staff/div/div/div[1]/mat-card/mat-card-actions/button")
    private WebElement receiveOrderItemButton;

    public OrderItemsPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void receiveOrderItemButtonClick() {
        Utilities.clickableWait(webDriver, this.receiveOrderItemButton, 1000).click();
    }

    public int getAllOrderItems(){
        return webDriver.findElements(By.xpath("/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-order-items-staff/div/div")).size();
    }
}
