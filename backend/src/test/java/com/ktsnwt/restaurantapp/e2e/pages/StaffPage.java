package com.ktsnwt.restaurantapp.e2e.pages;

import com.ktsnwt.restaurantapp.utils.Utilities;
import jdk.jshell.execution.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class StaffPage {
    private WebDriver driver;

    public StaffPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-employees-table/div/table/tbody/tr[1]/mat-cell/button[2]")
    private WebElement deleteButton;

    public void clickDeleteButton(){
        Utilities.clickableWait(driver,deleteButton,10).click();
    }

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav/div/mat-nav-list/a[7]")
    private WebElement staffNavbar;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-employees-table/div/mat-grid-list/div/mat-grid-tile[1]/div/mat-form-field/div/div[1]/div/input")
    private WebElement searchBox;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-employees-table/div/table/thead/tr/mat-header-cell/button")
    private WebElement newButton;

    @FindBy(xpath = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-user-modal/div[1]/mat-form-field/div/div[1]/div/input")
    private WebElement usernameInput;

    @FindBy(xpath = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-user-modal/div[3]/button")
    private WebElement addButton;

    public void clickAddButton(){
        Utilities.clickableWait(driver,addButton,10).click();
    }

    public void setUsernameInput(String s){
        WebElement webElement = getUsernameInput();
        webElement.clear();
        webElement.sendKeys(s);
    }


    @FindBy(xpath = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-user-modal/div[2]/mat-form-field[1]/div/div[1]/div/input")
    private WebElement firstNameInput;

    public void setFirstNameInput(String s){
        WebElement webElement = getFirstNameInput();
        webElement.clear();
        webElement.sendKeys(s);
    }

    @FindBy(xpath = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-user-modal/div[2]/mat-form-field[2]/div/div[1]/div/input")
    private WebElement lastNameInput;

    public void setLastNameInput(String s){
        WebElement webElement = getLastNameInput();
        webElement.clear();
        webElement.sendKeys(s);
    }

    @FindBy(xpath = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-user-modal/div[2]/mat-form-field[3]/div/div[1]/div/input")
    private WebElement emailAddressInput;

    public void setEmailAddressInput(String s){
        WebElement webElement = getEmailAddressInput();
        webElement.clear();
        webElement.sendKeys(s);
    }

    @FindBy(xpath = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-user-modal/div[2]/div[2]/mat-form-field/div/div[1]/div/mat-select")
    private WebElement roleSelector;

    public void clickRoleSelector(){
        Utilities.clickableWait(driver, roleSelector, 10).click();
    }

    @FindBy(xpath = "/html/body/div[3]/div[4]/div/div/div/mat-option[3]")
    private WebElement selectedRole;

    public void clickSelectedRole(){
        Utilities.clickableWait(driver, selectedRole, 10).click();
    }

    @FindBy(xpath = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-user-modal/div[2]/mat-form-field[4]/div/div[1]/div/input")
    private WebElement wageInput;

    public void setWageInput(String s){
        WebElement webElement = getWageInput();
        webElement.clear();
        webElement.sendKeys(s);
    }

    @FindBy(xpath = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-user-modal/div[2]/mat-form-field[5]/div/div[1]/div/input")
    private WebElement phoneInput;

    public void setPhoneInput(String s){
        WebElement webElement = getPhoneInput();
        webElement.clear();
        webElement.sendKeys(s);
    }



    public void clickNewButton(){
        Utilities.clickableWait(driver, newButton, 10).click();
    }

    public WebElement getNewButton() {
        return newButton;
    }

    public void setNewButton(WebElement newButton) {
        this.newButton = newButton;
    }

    public List<WebElement> getRows() {
        return rows;
    }

    public Integer rowsCount() {
        return Utilities.visibilityWait(driver, By.className("my-table-row"),10).size();
    }

    public WebElement getUsernameInput() {
        return usernameInput;
    }

    public void setUsernameInput(WebElement usernameInput) {
        this.usernameInput = usernameInput;
    }

    public WebElement getFirstNameInput() {
        return firstNameInput;
    }

    public void setFirstNameInput(WebElement firstNameInput) {
        this.firstNameInput = firstNameInput;
    }

    public WebElement getLastNameInput() {
        return lastNameInput;
    }

    public void setLastNameInput(WebElement lastNameInput) {
        this.lastNameInput = lastNameInput;
    }

    public WebElement getEmailAddressInput() {
        return emailAddressInput;
    }

    public void setEmailAddressInput(WebElement emailAddressInput) {
        this.emailAddressInput = emailAddressInput;
    }

    public WebElement getRoleSelector() {
        return roleSelector;
    }

    public void setRoleSelector(WebElement roleSelector) {
        this.roleSelector = roleSelector;
    }

    public WebElement getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(WebElement selectedRole) {
        this.selectedRole = selectedRole;
    }

    public WebElement getWageInput() {
        return wageInput;
    }

    public void setWageInput(WebElement wageInput) {
        this.wageInput = wageInput;
    }

    public WebElement getPhoneInput() {
        return phoneInput;
    }

    public void setPhoneInput(WebElement phoneInput) {
        this.phoneInput = phoneInput;
    }

    public void setRows(List<WebElement> rows) {
        this.rows = rows;
    }

    @FindBy(className = "my-table-row")
    private List<WebElement> rows;

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getStaffNavbar() {
        return staffNavbar;
    }

    public void setStaffNavbar(WebElement staffNavbar) {
        this.staffNavbar = staffNavbar;
    }

    public WebElement getSearchBox() {
        return searchBox;
    }

    public void setSearchBox(WebElement searchBox) {
        this.searchBox = searchBox;
    }

    public void clickStaffNavbar(){
        Utilities.clickableWait(driver, staffNavbar, 10).click();
    }

    public void setSearchInput(String s){
        WebElement webElement = getSearchBox();
        webElement.clear();
        webElement.sendKeys(s);
    }


}
