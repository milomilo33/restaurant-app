package com.ktsnwt.restaurantapp.e2e.tests;

import com.ktsnwt.restaurantapp.e2e.pages.LoginPage;
import com.ktsnwt.restaurantapp.e2e.pages.ReportPage;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class ManagerReportSelenium {
    private static WebDriver driver;

    private static LoginPage loginPage;

    private static ReportPage reportPage;

    @BeforeAll
    public static void  setupSelenium() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        // maximize window
        driver.manage().window().maximize();
        // navigate
        driver.navigate().to("http://localhost:4200/login");

        loginPage = PageFactory.initElements(driver, LoginPage.class);

        reportPage = PageFactory.initElements(driver, ReportPage.class);

    }

    @Test
    public void ManagerReportTest() throws InterruptedException {
        loginPage.setUsernameInput("manager1");
        loginPage.setPasswordInput("password1");
        loginPage.loginButtonClick();

        Thread.sleep(500);
        Assertions.assertEquals(driver.getCurrentUrl(), "http://localhost:4200/manager/reports");

        reportPage.roleButtonClick();

        Assertions.assertTrue(reportPage.diagramPresent());

    }


        @AfterAll
    public static void closeSelenium() {
        // Shutdown the driver
        driver.quit();
    }
}
