package com.ktsnwt.restaurantapp.e2e.tests;

import com.ktsnwt.restaurantapp.e2e.pages.LoginPage;
import com.ktsnwt.restaurantapp.e2e.pages.StaffPage;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ManagerStaffSeleniumTest {

    private static WebDriver driver;

    private static LoginPage loginPage;

    private static StaffPage staffPage;


    @BeforeAll
    public static void  setupSelenium() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        // maximize window
        driver.manage().window().maximize();
        // navigate
        driver.navigate().to("http://localhost:4200/login");

        loginPage = PageFactory.initElements(driver, LoginPage.class);

        staffPage = PageFactory.initElements(driver, StaffPage.class);

    }

    @Test
    @Order(1)
    public void ViewEmployeesTest() throws InterruptedException {
        loginPage.setUsernameInput("manager1");
        loginPage.setPasswordInput("password1");
        loginPage.loginButtonClick();

        staffPage.clickStaffNavbar();

        Assertions.assertEquals("http://localhost:4200/manager/employees", driver.getCurrentUrl());

        Assertions.assertEquals(7, staffPage.rowsCount());
        staffPage.setSearchInput("asdasdasjhdasjasd");
        //Assertions.assertTrue(staffPage.getEmployeeTotal().getText().contains("0"));
        Thread.sleep(500);
        Assertions.assertEquals(0, (int) staffPage.getRows().size());

        staffPage.setSearchInput("");

//        //staffPage.setUsernameInput("new_username");
//        staffPage.setFirstNameInput("MyFirstName");
//        staffPage.setLastNameInput("MyLastName");
//        staffPage.setEmailAddressInput("email@email.last");
//        staffPage.clickRoleSelector();
//        staffPage.clickSelectedRole();
//        staffPage.setWageInput("4000");
//        staffPage.setPhoneInput("123456789");
//        staffPage.clickAddButton();

    }

    @Test
    @Order(2)
    public void DeleteEmployeeTest() throws InterruptedException {
        loginPage.setUsernameInput("manager1");
        loginPage.setPasswordInput("password1");
        loginPage.loginButtonClick();

        staffPage.clickStaffNavbar();

        Assertions.assertEquals("http://localhost:4200/manager/employees", driver.getCurrentUrl());

        Assertions.assertEquals(7, staffPage.rowsCount());

        staffPage.clickDeleteButton();

        Thread.sleep(500);
        Assertions.assertEquals(6, staffPage.rowsCount());
    }


    @AfterAll
    public static void closeSelenium() {
        // Shutdown the driver
        driver.quit();
    }
}
