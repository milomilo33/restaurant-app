package com.ktsnwt.restaurantapp.e2e.pages;

import com.ktsnwt.restaurantapp.utils.Utilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ManagerHomePage {
    WebDriver webDriver;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav/div/mat-nav-list/a[6]")
    private WebElement menuViewTab;

    public ManagerHomePage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void menuViewTabClick() {
        Utilities.clickableWait(webDriver, this.menuViewTab, 10).click();
    }
}
