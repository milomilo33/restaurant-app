package com.ktsnwt.restaurantapp.e2e.pages;

import com.ktsnwt.restaurantapp.utils.Utilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WaiterHomePage {
    WebDriver webDriver;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav/div/mat-nav-list/a[4]")
    private WebElement ordersTab;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav/div/mat-nav-list/a[5]")
    private WebElement orderItemsTab;

    public WaiterHomePage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void ordersTabClick() {
        Utilities.clickableWait(webDriver, this.ordersTab, 100000).click();
    }
    public void orderItemsTabClick() {
        Utilities.clickableWait(webDriver, this.orderItemsTab, 100000).click();
    }
}
