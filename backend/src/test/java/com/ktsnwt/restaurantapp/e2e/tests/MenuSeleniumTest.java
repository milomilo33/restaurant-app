package com.ktsnwt.restaurantapp.e2e.tests;

import com.ktsnwt.restaurantapp.e2e.pages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class MenuSeleniumTest {
    private static final String BASE_URL = "http://localhost:4200";

    private WebDriver webDriver;

    private LoginPage loginPage;
    private ManagerHomePage managerHomePage;
    private MenusPage menusPage;
    private EditMenuPage editMenuPage;

    @Before
    public void setupSelenium(){
        // instantiate browser
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        webDriver = new ChromeDriver();
        // maximize window
        webDriver.manage().window().maximize();
        // navigate
        webDriver.navigate().to(BASE_URL + "/login");

        loginPage = PageFactory.initElements(webDriver, LoginPage.class);
        managerHomePage = PageFactory.initElements(webDriver, ManagerHomePage.class);
        menusPage = PageFactory.initElements(webDriver, MenusPage.class);
        editMenuPage = PageFactory.initElements(webDriver, EditMenuPage.class);
    }

    @Test
    public void deleteMenu() {
        loginPage.setUsernameInput("manager1");
        loginPage.setPasswordInput("password1");
        loginPage.loginButtonClick();
        managerHomePage.menuViewTabClick();
        Assertions.assertEquals(BASE_URL + "/manager/menuview", webDriver.getCurrentUrl());

        menusPage.deleteMenuButtonClick();
        menusPage.menuCardVisibility();
        Assertions.assertEquals(1, menusPage.numOfMenus());
    }

    @Test
    public void editMenu() {
        loginPage.setUsernameInput("manager1");
        loginPage.setPasswordInput("password1");
        loginPage.loginButtonClick();
        managerHomePage.menuViewTabClick();
        Assertions.assertEquals(BASE_URL + "/manager/menuview", webDriver.getCurrentUrl());

        menusPage.editMenuButtonClick();
        editMenuPage.setTextInput("NEW_NAME");
        editMenuPage.updateMenuButtonClick();
        menusPage.menuCardVisibility();
        Assertions.assertTrue(menusPage.menuCardTitleCheck("NEW_NAME"));
    }

    @After
    public void closeSelenium() {
        // Shutdown the browser
        webDriver.quit();
    }
}
