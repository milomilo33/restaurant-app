package com.ktsnwt.restaurantapp.e2e.pages;

import com.ktsnwt.restaurantapp.utils.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ReportPage {
    private WebDriver webDriver;

    @FindBy(id = "roleButton")
    private WebElement roleButton;



    public Boolean diagramPresent(){
        return Utilities.isPresent(webDriver, By.xpath("/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-manager-reports/mat-grid-list[2]/div/div/mat-grid-tile[2]/div/ngx-charts-bar-vertical"));
    }

    public void roleButtonClick(){
        Utilities.clickableWait(webDriver,roleButton,10).click();
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public ReportPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }
}
