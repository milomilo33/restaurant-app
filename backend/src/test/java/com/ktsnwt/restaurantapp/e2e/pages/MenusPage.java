package com.ktsnwt.restaurantapp.e2e.pages;

import com.ktsnwt.restaurantapp.utils.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MenusPage {
    WebDriver webDriver;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-menu-view/div/div/div[1]/mat-card/mat-card-actions/button[2]")
    private WebElement deleteMenuButton;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-menu-view/div/div/div[1]")
    private WebElement menuCard;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-menu-view/div/div/div[1]/mat-card/mat-card-actions/button[1]")
    private WebElement editMenuButton;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-menu-view/div/div/div[1]/mat-card/mat-card-header/div/mat-card-title")
    private WebElement menuCardTitle;

    public MenusPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void deleteMenuButtonClick() {
        Utilities.clickableWait(webDriver, this.deleteMenuButton, 10).click();
    }

    public void menuCardVisibility() {
        Utilities.visibilityWait(webDriver,menuCard,10);
    }

    public int numOfMenus (){
        return webDriver.findElements(By.xpath("/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-menu-view/div/div")).size();
    }

    public void editMenuButtonClick() {
        Utilities.clickableWait(webDriver, this.editMenuButton, 10).click();
    }

    public boolean menuCardTitleCheck(String title) {
        return Utilities.textWait(webDriver,this.menuCardTitle, title, 10);
    }
}
