package com.ktsnwt.restaurantapp.e2e.tests;

import com.ktsnwt.restaurantapp.e2e.pages.LoginPage;
import com.ktsnwt.restaurantapp.e2e.pages.OrdersPage;
import com.ktsnwt.restaurantapp.e2e.pages.WaiterHomePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

@SpringBootTest
public class OrderSeleniumTest {
    private static final String BASE_URL = "http://localhost:4200";

    private WebDriver webDriver;

    private LoginPage loginPage;
    private WaiterHomePage waiterHomePage;
    private OrdersPage ordersPage;

    @Before
    public void setupSelenium(){
        // instantiate browser
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        webDriver = new ChromeDriver();
        // maximize window
        webDriver.manage().window().maximize();
        // navigate
        webDriver.navigate().to(BASE_URL + "/login");

        loginPage = PageFactory.initElements(webDriver, LoginPage.class);
        waiterHomePage = PageFactory.initElements(webDriver, WaiterHomePage.class);
        ordersPage = PageFactory.initElements(webDriver, OrdersPage.class);
    }

    @Test
    public void searchOrders(){
        loginPage.setUsernameInput("waiter1");
        loginPage.setPasswordInput("password1");
        loginPage.loginButtonClick();
        waiterHomePage.ordersTabClick();
        Assertions.assertEquals(BASE_URL + "/orders", webDriver.getCurrentUrl());
        ordersPage.completeOrdersToggleButtonClick();
        ordersPage.searchButtonClick();
        Assertions.assertEquals(1, ordersPage.searchedOrders());
    }

    @Test
    public void chargeOrder() throws InterruptedException {
        loginPage.setUsernameInput("waiter1");
        loginPage.setPasswordInput("password1");
        loginPage.loginButtonClick();
        waiterHomePage.ordersTabClick();
        Assertions.assertEquals(BASE_URL + "/orders", webDriver.getCurrentUrl());
        ordersPage.chargeOrderButtonClick();
        Thread.sleep(3000);
        //ordersPage.waitUntilOrderIsComplete();
        Assertions.assertTrue(ordersPage.checkIfOrderComplete());
    }

    @After
    public void closeSelenium() {
        // Shutdown the browser
        webDriver.quit();
    }
}
