package com.ktsnwt.restaurantapp.e2e.pages;

import com.ktsnwt.restaurantapp.utils.Utilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {
    private WebDriver webDriver;

    @FindBy(xpath = "//*[@id=\"mat-input-0\"]")
    private WebElement usernameInput;

    @FindBy(xpath = "//*[@id=\"mat-input-1\"]")
    private WebElement passwordInput;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-login/div/mat-card/form/button")
    private WebElement loginButton;

    public LoginPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public WebElement getUsernameInput() {
        return usernameInput;
    }

    public void setUsernameInput(String usernameInput) {
        WebElement webElement = getUsernameInput();
        webElement.clear();
        webElement.sendKeys(usernameInput);
    }

    public WebElement getPasswordInput() {
        return passwordInput;
    }

    public void setPasswordInput(String passwordInput) {
        WebElement webElement = getPasswordInput();
        webElement.clear();
        webElement.sendKeys(passwordInput);
    }

    public void loginButtonClick() {
        Utilities.clickableWait(webDriver, this.loginButton, 10).click();
    }
}
