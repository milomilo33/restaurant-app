package com.ktsnwt.restaurantapp.e2e.pages;

import com.ktsnwt.restaurantapp.utils.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OrdersPage {
    WebDriver webDriver;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav/div/mat-nav-list/a[4]")
    private WebElement completeOrdersToggleButton;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-all-orders/div/mat-grid-list/div/mat-grid-tile[4]/div/button")
    private WebElement searchButton;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-all-orders/div/div/div[1]/mat-card/mat-card-actions/button[2]")
    private WebElement chargeOrderButton;


    public OrdersPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void completeOrdersToggleButtonClick() {
        Utilities.clickableWait(webDriver, this.completeOrdersToggleButton, 100000).click();
    }

    public void searchButtonClick() {
        Utilities.clickableWait(webDriver, this.searchButton, 100000).click();
    }

    public void chargeOrderButtonClick() {
        Utilities.clickableWait(webDriver, this.chargeOrderButton, 100000).click();
    }

    public int searchedOrders(){
        return webDriver.findElements(By.xpath("/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-all-orders/div/div")).size();
    }

    public boolean checkIfOrderComplete(){
        return  webDriver.findElement(By.xpath("/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-all-orders/div/div/div[1]/mat-card/mat-card-content/p[2]")).getText().equals("Order is completed") ||
                webDriver.findElement(By.xpath("/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-all-orders/div/div/div[3]/mat-card/mat-card-content/p[2]")).getText().equals("Order is completed");
    }
/*
    public void waitUntilOrderIsComplete(){
        Utilities.visibilityWait(webDriver, webDriver.findElement(By.xpath("/html/body/app-root/app-navbar/mat-sidenav-container/mat-sidenav-content/app-all-orders/div/div/div[1]/mat-card/mat-card-content/p[2]")), 30000);
    }
 */
}
