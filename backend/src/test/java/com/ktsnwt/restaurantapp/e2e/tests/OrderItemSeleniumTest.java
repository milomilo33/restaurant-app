package com.ktsnwt.restaurantapp.e2e.tests;

import com.ktsnwt.restaurantapp.e2e.pages.LoginPage;
import com.ktsnwt.restaurantapp.e2e.pages.OrderItemsPage;
import com.ktsnwt.restaurantapp.e2e.pages.OrdersPage;
import com.ktsnwt.restaurantapp.e2e.pages.WaiterHomePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class OrderItemSeleniumTest {
    private static final String BASE_URL = "http://localhost:4200";

    private WebDriver webDriver;

    private LoginPage loginPage;
    private WaiterHomePage waiterHomePage;
    private OrderItemsPage orderItemsPage;

    @Before
    public void setupSelenium(){
        // instantiate browser
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        webDriver = new ChromeDriver();
        // maximize window
        webDriver.manage().window().maximize();
        // navigate
        webDriver.navigate().to(BASE_URL + "/login");

        loginPage = PageFactory.initElements(webDriver, LoginPage.class);
        waiterHomePage = PageFactory.initElements(webDriver, WaiterHomePage.class);
        orderItemsPage = PageFactory.initElements(webDriver, OrderItemsPage.class);
    }

    @Test
    public void receiveOrderItem() {
        loginPage.setUsernameInput("waiter1");
        loginPage.setPasswordInput("password1");
        loginPage.loginButtonClick();
        waiterHomePage.orderItemsTabClick();
        Assertions.assertEquals(BASE_URL + "/order-items/staff", webDriver.getCurrentUrl());
        orderItemsPage.receiveOrderItemButtonClick();
        Assertions.assertEquals(1, orderItemsPage.getAllOrderItems());
    }

    @After
    public void closeSelenium() {
        // Shutdown the browser
        webDriver.quit();
    }
}
