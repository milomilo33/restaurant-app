package com.ktsnwt.restaurantapp.repository;

import com.ktsnwt.restaurantapp.constants.UserConstants;
import com.ktsnwt.restaurantapp.model.Menu;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class MenuRepositoryIntegrationTest {
    @Autowired
    private MenuRepository menuRepository;

    @Test
    public void testSearchSortFilterAnyActive(){
        Page<Menu> menus = menuRepository.searchSortFilter("%i%", null, PageRequest.of(0,10));
        for (Menu m : menus){
            Assertions.assertTrue(m.getName().toLowerCase().contains("i"));
            Assertions.assertFalse(m.isDeleted());
        }
        Assertions.assertEquals(0, menus.getNumber());
        Assertions.assertEquals(10, menus.getSize());
    }

    @Test
    public void testSearchSortFilterActive(){
        Page<Menu> menus = menuRepository.searchSortFilter("%%", true, PageRequest.of(0,10));
        for (Menu m : menus){
            Assertions.assertTrue(m.isActive());
            Assertions.assertFalse(m.isDeleted());
        }
        Assertions.assertEquals(0, menus.getNumber());
        Assertions.assertEquals(10, menus.getSize());
        System.out.println(menus.getTotalElements());
    }

    @Test
    public void testSearchSortFilterInactive(){
        Page<Menu> menus = menuRepository.searchSortFilter("%%", false, PageRequest.of(0,10));
        for (Menu m : menus){
            Assertions.assertFalse(m.isActive());
            Assertions.assertFalse(m.isDeleted());
        }
        Assertions.assertEquals(0, menus.getNumber());
        Assertions.assertEquals(10, menus.getSize());
    }

}
