package com.ktsnwt.restaurantapp.repository;


import com.ktsnwt.restaurantapp.constants.LogConstants;
import com.ktsnwt.restaurantapp.model.Log;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.stream.Collectors;

@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class LogRepositoryTest {

    @Autowired
    private LogRepository logRepository;

    @Test
    public void testFindByNameAndDates(){
        List<Log> logs = logRepository.findByMenuItemNameContainsIgnoreCaseAndTimestampGreaterThanEqualAndTimestampLessThanEqualOrderByTimestampAsc(
                LogConstants.DISN_NAME,
                LogConstants.START_DATE,
                LogConstants.END_DATE
        );
        for(Log l : logs){
            Assertions.assertTrue(l.getTimestamp().isAfter(LogConstants.START_DATE.minusDays(1)));
            Assertions.assertTrue(l.getTimestamp().isBefore(LogConstants.END_DATE.plusDays(1)));
            Assertions.assertTrue(l.getMenuItemName().equalsIgnoreCase(LogConstants.DISN_NAME));
        }
    }

    @Test
    public void testDistinctNames(){
        List<String> names = logRepository.findNames();
        Assertions.assertEquals(names.size(), names.stream().distinct().count());
    }

}
