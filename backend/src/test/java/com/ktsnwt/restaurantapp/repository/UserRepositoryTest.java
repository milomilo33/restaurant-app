package com.ktsnwt.restaurantapp.repository;



import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.ktsnwt.restaurantapp.constants.UserConstants;
import com.ktsnwt.restaurantapp.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;


@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindByMail(){
        Assertions.assertEquals(userRepository.findByMail(UserConstants.MAIL)
                .get()
                .getMail(), UserConstants.MAIL);
    }

    @Test
    public void testFindByUsername(){
        Assertions.assertEquals(userRepository.findByUsername(UserConstants.USERNAME)
                .getMail(), UserConstants.MAIL);
    }

    @Test
    public void testFindByNotRole(){
        List<User> users = userRepository.findByRoleNotContainingIgnoreCaseAndDeletedIsFalse(UserConstants.WAITER_ROLE);

        for(User u : users){
            Assertions.assertFalse(u.getRole().contains(UserConstants.WAITER_ROLE));
        }
    }

    @Test
    public void testFindByNotRoleNotRole(){
        List<User> users = userRepository.findByRoleNotContainingIgnoreCaseAndRoleNotContainingIgnoreCaseAndDeletedIsFalse(UserConstants.WAITER_ROLE, UserConstants.ADMIN_ROLE);

        for(User u : users){
            Assertions.assertFalse(u.getRole().contains(UserConstants.WAITER_ROLE));
            Assertions.assertFalse(u.getRole().contains(UserConstants.ADMIN_ROLE));
        }
    }

    @Test
    public void testFindByUsernameAndNotRole(){
        Assertions.assertEquals(UserConstants.USERNAME, userRepository.findByUsernameIsAndRoleNotContainingIgnoreCaseAndDeletedIsFalse(
                UserConstants.USERNAME,
                UserConstants.WAITER_ROLE
        ).get().getUsername());
    }

    @Test
    public void testFindByUsernameAndNotRoleNotRole(){
        Assertions.assertEquals(UserConstants.USERNAME, userRepository.findByUsernameIsAndRoleNotContainingAndRoleNotContainingIgnoreCaseAndDeletedIsFalse(
                UserConstants.USERNAME,
                UserConstants.WAITER_ROLE,
                UserConstants.ADMIN_ROLE
        ).get().getUsername());
    }

    @Test
    public void testBigQueryGreaterThan(){
        List<User> users = userRepository.bigQueryGreaterEqual(UserConstants.ADMIN_ROLE, UserConstants.SEARCH, "BARMAN", 30.0, PageRequest.of(1,10));
        for (User u : users){
            Assertions.assertFalse(u.getRole().contains(UserConstants.ADMIN_ROLE));
            Assertions.assertTrue(u.getUsername().toUpperCase().contains(UserConstants.SEARCH.toUpperCase())
            || u.getName().toUpperCase().contains(UserConstants.SEARCH.toUpperCase())
            || u.getSurname().toUpperCase().contains(UserConstants.SEARCH.toUpperCase())
            || u.getMail().toUpperCase().contains(UserConstants.SEARCH.toUpperCase()));
            Assertions.assertEquals("BARMAN", u.getRole());
            Assertions.assertTrue(u.getWage() > 30.0);
        }
        Assertions.assertTrue(users.size() <= 10);

    }

    @Test
    public void testBigQueryLesserThan(){
        List<User> users = userRepository.bigQueryLesserEqual(UserConstants.ADMIN_ROLE, UserConstants.SEARCH, "BARMAN", 10000.0, PageRequest.of(1,10));
        for (User u : users){
            Assertions.assertFalse(u.getRole().contains(UserConstants.ADMIN_ROLE));
            Assertions.assertTrue(u.getUsername().toUpperCase().contains(UserConstants.SEARCH.toUpperCase())
                    || u.getName().toUpperCase().contains(UserConstants.SEARCH.toUpperCase())
                    || u.getSurname().toUpperCase().contains(UserConstants.SEARCH.toUpperCase())
                    || u.getMail().toUpperCase().contains(UserConstants.SEARCH.toUpperCase()));
            Assertions.assertEquals("BARMAN", u.getRole());
            Assertions.assertTrue(u.getWage() < 10000.0);
        }
        Assertions.assertTrue(users.size() <= 10);

    }

}
