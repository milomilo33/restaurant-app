package com.ktsnwt.restaurantapp.repository;

import com.ktsnwt.restaurantapp.dto.MenuItemInMenuDTO;
import com.ktsnwt.restaurantapp.model.Menu;
import com.ktsnwt.restaurantapp.model.MenuItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource("classpath:application.properties")
@SpringBootTest
public class MenuItemRepositoryIntegrationTest {
    @Autowired
    private MenuItemRepository menuItemRepository;

    @Test
    public void testSearchSortFilterAny(){
        Page<MenuItem> menuItems = menuItemRepository.searchSortFilter("%a%", "%%", null, null, "%s%", PageRequest.of(0,10));
        for (MenuItem mi : menuItems){
            Assertions.assertTrue(mi.getName().toLowerCase().contains("a"));
            Assertions.assertFalse(mi.isDeleted());
            Assertions.assertTrue(mi.getCategory().toLowerCase().contains("s"));
        }
        Assertions.assertEquals(0, menuItems.getNumber());
        Assertions.assertEquals(10, menuItems.getSize());
    }

    @Test
    public void testSearchSortFilterFood(){
        Page<MenuItem> menuItems = menuItemRepository.searchSortFilter("%a%", "%%", true, null, "%s%", PageRequest.of(0,10));
        for (MenuItem mi : menuItems){
            Assertions.assertTrue(mi.getName().contains("a"));
            Assertions.assertFalse(mi.isDeleted());
            Assertions.assertTrue(mi.getCategory().toLowerCase().contains("s"));
            Assertions.assertTrue(mi.isFood());
        }
        Assertions.assertEquals(0, menuItems.getNumber());
        Assertions.assertEquals(10, menuItems.getSize());
    }

    @Test
    public void testSearchSortFilterDrink(){
        Page<MenuItem> menuItems = menuItemRepository.searchSortFilter("%a%", "%%", false, null, "%s%", PageRequest.of(0,10));
        for (MenuItem mi : menuItems){
            Assertions.assertTrue(mi.getName().toLowerCase().contains("a"));
            Assertions.assertFalse(mi.isDeleted());
            Assertions.assertTrue(mi.getCategory().toLowerCase().contains("s"));
            Assertions.assertFalse(mi.isFood());
        }
        Assertions.assertEquals(0, menuItems.getNumber());
        Assertions.assertEquals(10, menuItems.getSize());
    }

    @Test
    public void testSearchSortFilterAvailable(){
        Page<MenuItem> menuItems = menuItemRepository.searchSortFilter("%a%", "%%", null, true, "%s%", PageRequest.of(0,10));
        for (MenuItem mi : menuItems){
            Assertions.assertTrue(mi.getName().toLowerCase().contains("a"));
            Assertions.assertFalse(mi.isDeleted());
            Assertions.assertTrue(mi.getCategory().toLowerCase().contains("s"));
            Assertions.assertTrue(mi.isAvailable());
        }
        Assertions.assertEquals(0, menuItems.getNumber());
        Assertions.assertEquals(10, menuItems.getSize());
    }

    @Test
    public void testSearchSortFilterUnavailable(){
        Page<MenuItem> menuItems = menuItemRepository.searchSortFilter("%a%", "%%", null, false, "%s%", PageRequest.of(0,10));
        for (MenuItem mi : menuItems){
            Assertions.assertTrue(mi.getName().toLowerCase().contains("a"));
            Assertions.assertFalse(mi.isDeleted());
            Assertions.assertTrue(mi.getCategory().toLowerCase().contains("s"));
            Assertions.assertFalse(mi.isAvailable());
        }
        Assertions.assertEquals(0, menuItems.getNumber());
        Assertions.assertEquals(10, menuItems.getSize());
    }

//    @Test
//    public void testSearchSortFilterForMenuAny(){
//        Page<MenuItemInMenuDTO> menuItemsForMenu = menuItemRepository.searchSortFilterForMenuAny("%a%", "%%", null, null, "%s%", 1L, PageRequest.of(0,10));
//        for (MenuItemInMenuDTO miim : menuItemsForMenu){
//            Assertions.assertTrue(miim.getName().toLowerCase().contains("a"));
//            Assertions.assertFalse(miim.isDeleted());
//            Assertions.assertTrue(miim.getCategory().toLowerCase().contains("s"));
//            Assertions.assertTrue(miim.getMenuId() == null || miim.getMenuId().equals(1L));
//            if (miim.getMenuId() == null)
//                Assertions.assertNull(miim.getPriceInMenu());
//            else
//                Assertions.assertNotNull(miim.getPriceInMenu());
//        }
//        Assertions.assertEquals(0, menuItemsForMenu.getNumber());
//        Assertions.assertEquals(10, menuItemsForMenu.getSize());
//    }
//
//    @Test
//    public void testSearchSortFilterForMenuInMenu(){
//        Page<MenuItemInMenuDTO> menuItemsInMenu = menuItemRepository.searchSortFilterForMenuIn("%a%", "%%", null, null, "%s%", 1L, PageRequest.of(0,10));
//        for (MenuItemInMenuDTO miim : menuItemsInMenu){
//            Assertions.assertTrue(miim.getName().toLowerCase().contains("a"));
//            Assertions.assertFalse(miim.isDeleted());
//            Assertions.assertTrue(miim.getCategory().toLowerCase().contains("s"));
//            Assertions.assertEquals(1L, (long) miim.getMenuId());
//            Assertions.assertNotNull(miim.getPriceInMenu());
//        }
//        Assertions.assertEquals(0, menuItemsInMenu.getNumber());
//        Assertions.assertEquals(10, menuItemsInMenu.getSize());
//    }
//
//    @Test
//    public void testSearchSortFilterForMenuNotInMenu(){
//        Page<MenuItemInMenuDTO> menuItemsInMenu = menuItemRepository.searchSortFilterForMenuNotIn("%a%", "%%", null, null, "%s%", 1L, PageRequest.of(0,10));
//        for (MenuItemInMenuDTO miim : menuItemsInMenu){
//            Assertions.assertTrue(miim.getName().toLowerCase().contains("a"));
//            Assertions.assertFalse(miim.isDeleted());
//            Assertions.assertTrue(miim.getCategory().toLowerCase().contains("s"));
//            Assertions.assertNull(miim.getMenuId());
//            Assertions.assertNull(miim.getPriceInMenu());
//        }
//        Assertions.assertEquals(0, menuItemsInMenu.getNumber());
//        Assertions.assertEquals(10, menuItemsInMenu.getSize());
//    }

}